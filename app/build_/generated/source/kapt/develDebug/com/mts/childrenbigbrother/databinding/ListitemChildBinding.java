package com.mts.childrenbigbrother.databinding;
import com.mts.childrenbigbrother.R;
import com.mts.childrenbigbrother.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
@javax.annotation.Generated("Android Data Binding")
public class ListitemChildBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.ivPhoto, 4);
    }
    // views
    @NonNull
    public final android.support.v7.widget.CardView cardView;
    @NonNull
    public final android.widget.ImageView ivPhoto;
    @NonNull
    public final android.widget.TextView tvAge;
    @NonNull
    public final android.widget.TextView tvGender;
    @NonNull
    public final android.widget.TextView tvName;
    // variables
    @Nullable
    private com.mts.childrenbigbrother.viewmodel.ChildViewModel mItemChildViewModel;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ListitemChildBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 3);
        final Object[] bindings = mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds);
        this.cardView = (android.support.v7.widget.CardView) bindings[0];
        this.cardView.setTag(null);
        this.ivPhoto = (android.widget.ImageView) bindings[4];
        this.tvAge = (android.widget.TextView) bindings[3];
        this.tvAge.setTag(null);
        this.tvGender = (android.widget.TextView) bindings[2];
        this.tvGender.setTag(null);
        this.tvName = (android.widget.TextView) bindings[1];
        this.tvName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable) {
        switch(variableId) {
            case BR.itemChildViewModel :
                setItemChildViewModel((com.mts.childrenbigbrother.viewmodel.ChildViewModel) variable);
                return true;
        }
        return false;
    }

    public void setItemChildViewModel(@Nullable com.mts.childrenbigbrother.viewmodel.ChildViewModel ItemChildViewModel) {
        this.mItemChildViewModel = ItemChildViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.itemChildViewModel);
        super.requestRebind();
    }
    @Nullable
    public com.mts.childrenbigbrother.viewmodel.ChildViewModel getItemChildViewModel() {
        return mItemChildViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemChildViewModelStrChildeAge((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeItemChildViewModelStrChildGenter((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeItemChildViewModelStrChildName((android.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemChildViewModelStrChildeAge(android.databinding.ObservableField<java.lang.String> ItemChildViewModelStrChildeAge, int fieldId) {
        switch (fieldId) {
            case BR._all: {
                synchronized(this) {
                        mDirtyFlags |= 0x1L;
                }
                return true;
            }
        }
        return false;
    }
    private boolean onChangeItemChildViewModelStrChildGenter(android.databinding.ObservableField<java.lang.String> ItemChildViewModelStrChildGenter, int fieldId) {
        switch (fieldId) {
            case BR._all: {
                synchronized(this) {
                        mDirtyFlags |= 0x2L;
                }
                return true;
            }
        }
        return false;
    }
    private boolean onChangeItemChildViewModelStrChildName(android.databinding.ObservableField<java.lang.String> ItemChildViewModelStrChildName, int fieldId) {
        switch (fieldId) {
            case BR._all: {
                synchronized(this) {
                        mDirtyFlags |= 0x4L;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemChildViewModelStrChildNameGet = null;
        android.databinding.ObservableField<java.lang.String> itemChildViewModelStrChildeAge = null;
        java.lang.String itemChildViewModelStrChildeAgeGet = null;
        android.databinding.ObservableField<java.lang.String> itemChildViewModelStrChildGenter = null;
        com.mts.childrenbigbrother.viewmodel.ChildViewModel itemChildViewModel = mItemChildViewModel;
        java.lang.String itemChildViewModelStrChildGenterGet = null;
        android.databinding.ObservableField<java.lang.String> itemChildViewModelStrChildName = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (itemChildViewModel != null) {
                        // read itemChildViewModel.strChildeAge
                        itemChildViewModelStrChildeAge = itemChildViewModel.getStrChildeAge();
                    }
                    updateRegistration(0, itemChildViewModelStrChildeAge);


                    if (itemChildViewModelStrChildeAge != null) {
                        // read itemChildViewModel.strChildeAge.get()
                        itemChildViewModelStrChildeAgeGet = itemChildViewModelStrChildeAge.get();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (itemChildViewModel != null) {
                        // read itemChildViewModel.strChildGenter
                        itemChildViewModelStrChildGenter = itemChildViewModel.getStrChildGenter();
                    }
                    updateRegistration(1, itemChildViewModelStrChildGenter);


                    if (itemChildViewModelStrChildGenter != null) {
                        // read itemChildViewModel.strChildGenter.get()
                        itemChildViewModelStrChildGenterGet = itemChildViewModelStrChildGenter.get();
                    }
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (itemChildViewModel != null) {
                        // read itemChildViewModel.strChildName
                        itemChildViewModelStrChildName = itemChildViewModel.getStrChildName();
                    }
                    updateRegistration(2, itemChildViewModelStrChildName);


                    if (itemChildViewModelStrChildName != null) {
                        // read itemChildViewModel.strChildName.get()
                        itemChildViewModelStrChildNameGet = itemChildViewModelStrChildName.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.set                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         