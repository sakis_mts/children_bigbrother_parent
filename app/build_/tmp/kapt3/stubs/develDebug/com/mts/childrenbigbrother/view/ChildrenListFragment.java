package com.mts.childrenbigbrother.view;

import java.lang.System;

/**
 * * Created by sakis on 2/18/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0006\u0010\u001a\u001a\u00020\u0017J\u0006\u0010\u001b\u001a\u00020\u0017J\u0016\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eJ\u0012\u0010\u001f\u001a\u00020\u00172\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J(\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010$\u001a\u0004\u0018\u00010%2\b\u0010&\u001a\u0004\u0018\u00010\'2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016R\u001c\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\n\u001a\u00020\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u0011X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006("}, d2 = {"Lcom/mts/childrenbigbrother/view/ChildrenListFragment;", "Landroid/support/v4/app/Fragment;", "()V", "fragmentNavChildrenListBinding", "error/NonExistentClass", "getFragmentNavChildrenListBinding", "()Lerror/NonExistentClass;", "setFragmentNavChildrenListBinding", "(Lerror/NonExistentClass;)V", "Lerror/NonExistentClass;", "navChildrenListViewModel", "Lcom/mts/childrenbigbrother/viewmodel/NavChildrenListViewModel;", "getNavChildrenListViewModel", "()Lcom/mts/childrenbigbrother/viewmodel/NavChildrenListViewModel;", "setNavChildrenListViewModel", "(Lcom/mts/childrenbigbrother/viewmodel/NavChildrenListViewModel;)V", "objChildrenAdapter", "Lcom/mts/childrenbigbrother/adapter/ChildListAdapter;", "getObjChildrenAdapter", "()Lcom/mts/childrenbigbrother/adapter/ChildListAdapter;", "setObjChildrenAdapter", "(Lcom/mts/childrenbigbrother/adapter/ChildListAdapter;)V", "addChild", "", "child", "Lcom/mts/childrenbigbrother/model/Child;", "initVars", "initViewModel", "objClicked", "index", "", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "app_develDebug"})
public final class ChildrenListFragment extends android.support.v4.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public error.NonExistentClass fragmentNavChildrenListBinding;
    @org.jetbrains.annotations.NotNull()
    public com.mts.childrenbigbrother.viewmodel.NavChildrenListViewModel navChildrenListViewModel;
    @org.jetbrains.annotations.NotNull()
    public com.mts.childrenbigbrother.adapter.ChildListAdapter objChildrenAdapter;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final error.NonExistentClass getFragmentNavChildrenListBinding() {
        return null;
    }
    
    public final void setFragmentNavChildrenListBinding(@org.jetbrains.annotations.NotNull()
    error.NonExistentClass p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mts.childrenbigbrother.viewmodel.NavChildrenListViewModel getNavChildrenListViewModel() {
        return null;
    }
    
    public final void setNavChildrenListViewModel(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.viewmodel.NavChildrenListViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mts.childrenbigbrother.adapter.ChildListAdapter getObjChildrenAdapter() {
        return null;
    }
    
    public final void setObjChildrenAdapter(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.adapter.ChildListAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.Nullable()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initViewModel() {
    }
    
    public final void initVars() {
    }
    
    public final void addChild(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.model.Child child) {
    }
    
    public final void objClicked(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.model.Child child, int index) {
    }
    
    public ChildrenListFragment() {
        super();
    }
}