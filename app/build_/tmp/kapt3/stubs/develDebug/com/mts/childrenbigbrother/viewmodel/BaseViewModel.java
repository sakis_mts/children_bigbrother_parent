package com.mts.childrenbigbrother.viewmodel;

import java.lang.System;

/**
 * * Created by sakis on 2/18/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010 \u001a\u00020!2\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0013R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u00020\u0013X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R(\u0010\u0018\u001a\u0010\u0012\f\u0012\n \u001b*\u0004\u0018\u00010\u001a0\u001a0\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001f\u00a8\u0006\""}, d2 = {"Lcom/mts/childrenbigbrother/viewmodel/BaseViewModel;", "", "()V", "activity", "Landroid/app/Activity;", "getActivity", "()Landroid/app/Activity;", "setActivity", "(Landroid/app/Activity;)V", "apiService", "error/NonExistentClass", "getApiService", "()Lerror/NonExistentClass;", "setApiService", "(Lerror/NonExistentClass;)V", "Lerror/NonExistentClass;", "compositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "fragment", "Landroid/support/v4/app/Fragment;", "getFragment", "()Landroid/support/v4/app/Fragment;", "setFragment", "(Landroid/support/v4/app/Fragment;)V", "obsProgrBar", "Landroid/databinding/ObservableField;", "", "kotlin.jvm.PlatformType", "getObsProgrBar", "()Landroid/databinding/ObservableField;", "setObsProgrBar", "(Landroid/databinding/ObservableField;)V", "initViewModel", "", "app_develDebug"})
public final class BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.Integer> obsProgrBar;
    private io.reactivex.disposables.CompositeDisposable compositeDisposable;
    @org.jetbrains.annotations.NotNull()
    public android.app.Activity activity;
    @org.jetbrains.annotations.NotNull()
    public android.support.v4.app.Fragment fragment;
    @org.jetbrains.annotations.NotNull()
    public error.NonExistentClass apiService;
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.Integer> getObsProgrBar() {
        return null;
    }
    
    public final void setObsProgrBar(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Activity getActivity() {
        return null;
    }
    
    public final void setActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.v4.app.Fragment getFragment() {
        return null;
    }
    
    public final void setFragment(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final error.NonExistentClass getApiService() {
        return null;
    }
    
    public final void setApiService(@org.jetbrains.annotations.NotNull()
    error.NonExistentClass p0) {
    }
    
    public final void initViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment fragment) {
    }
    
    public BaseViewModel() {
        super();
    }
}