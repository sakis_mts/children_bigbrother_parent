package com.mts.childrenbigbrother.application;

import java.lang.System;

/**
 * * Created by sakis on 2/11/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00002\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\u001e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0015"}, d2 = {"Lcom/mts/childrenbigbrother/application/ChildrenBigBrotherApp;", "Landroid/app/Application;", "()V", "retrofitComponent", "Lcom/sonin/soninandroidblankproject/dagger/AppComponent;", "getRetrofitComponent", "()Lcom/sonin/soninandroidblankproject/dagger/AppComponent;", "setRetrofitComponent", "(Lcom/sonin/soninandroidblankproject/dagger/AppComponent;)V", "initDagger", "app", "module", "Lcom/sonin/soninandroidblankproject/dagger/RetrofitModule;", "onCreate", "", "updateDagger", "accessToken", "", "strUsername", "accountType", "Companion", "app_develDebug"})
public final class ChildrenBigBrotherApp extends android.app.Application {
    @org.jetbrains.annotations.Nullable()
    private com.sonin.soninandroidblankproject.dagger.AppComponent retrofitComponent;
    @org.jetbrains.annotations.NotNull()
    private static com.mts.childrenbigbrother.application.ChildrenBigBrotherApp instance;
    private static boolean blntokenRefresh;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String BDK_FIREBASE_ID = "FirebaseToken";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String PREFS_NAME = "Device Prefs";
    private static int intUnreadNotifications;
    private static final long networkTimeout = 30L;
    public static final com.mts.childrenbigbrother.application.ChildrenBigBrotherApp.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final com.sonin.soninandroidblankproject.dagger.AppComponent getRetrofitComponent() {
        return null;
    }
    
    public final void setRetrofitComponent(@org.jetbrains.annotations.Nullable()
    com.sonin.soninandroidblankproject.dagger.AppComponent p0) {
    }
    
    private final com.sonin.soninandroidblankproject.dagger.AppComponent initDagger(com.mts.childrenbigbrother.application.ChildrenBigBrotherApp app, com.sonin.soninandroidblankproject.dagger.RetrofitModule module) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public final void updateDagger(@org.jetbrains.annotations.NotNull()
    java.lang.String accessToken, @org.jetbrains.annotations.NotNull()
    java.lang.String strUsername, @org.jetbrains.annotations.NotNull()
    java.lang.String accountType) {
    }
    
    public ChildrenBigBrotherApp() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@BX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u001dX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001f\u00a8\u0006 "}, d2 = {"Lcom/mts/childrenbigbrother/application/ChildrenBigBrotherApp$Companion;", "", "()V", "BDK_FIREBASE_ID", "", "getBDK_FIREBASE_ID", "()Ljava/lang/String;", "PREFS_NAME", "getPREFS_NAME", "blntokenRefresh", "", "getBlntokenRefresh", "()Z", "setBlntokenRefresh", "(Z)V", "<set-?>", "Lcom/mts/childrenbigbrother/application/ChildrenBigBrotherApp;", "instance", "getInstance", "()Lcom/mts/childrenbigbrother/application/ChildrenBigBrotherApp;", "setInstance", "(Lcom/mts/childrenbigbrother/application/ChildrenBigBrotherApp;)V", "intUnreadNotifications", "", "getIntUnreadNotifications", "()I", "setIntUnreadNotifications", "(I)V", "networkTimeout", "", "getNetworkTimeout", "()J", "app_develDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mts.childrenbigbrother.application.ChildrenBigBrotherApp getInstance() {
            return null;
        }
        
        private final void setInstance(com.mts.childrenbigbrother.application.ChildrenBigBrotherApp p0) {
        }
        
        public final boolean getBlntokenRefresh() {
            return false;
        }
        
        public final void setBlntokenRefresh(boolean p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getBDK_FIREBASE_ID() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getPREFS_NAME() {
            return null;
        }
        
        public final int getIntUnreadNotifications() {
            return 0;
        }
        
        public final void setIntUnreadNotifications(int p0) {
        }
        
        public final long getNetworkTimeout() {
            return 0L;
        }
        
        private Companion() {
            super();
        }
    }
}