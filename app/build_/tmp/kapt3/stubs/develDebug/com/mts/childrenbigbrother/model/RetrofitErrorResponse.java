package com.mts.childrenbigbrother.model;

import java.lang.System;

/**
 * * Created by sakis on 2/10/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\n\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R \u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0005\u00a8\u0006\u0012"}, d2 = {"Lcom/mts/childrenbigbrother/model/RetrofitErrorResponse;", "", "arrMessages", "", "", "(Ljava/util/List;)V", "getArrMessages", "()Ljava/util/List;", "setArrMessages", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "app_develDebug"})
public final class RetrofitErrorResponse {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<java.lang.String> arrMessages;
    public static final com.mts.childrenbigbrother.model.RetrofitErrorResponse.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getArrMessages() {
        return null;
    }
    
    public final void setArrMessages(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> p0) {
    }
    
    public RetrofitErrorResponse(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> arrMessages) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component1() {
        return null;
    }
    
    /**
     * * Created by sakis on 2/10/2018.
     */
    @org.jetbrains.annotations.NotNull()
    public final com.mts.childrenbigbrother.model.RetrofitErrorResponse copy(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> arrMessages) {
        return null;
    }
    
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(java.lang.Object p0) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\t\u00a8\u0006\n"}, d2 = {"Lcom/mts/childrenbigbrother/model/RetrofitErrorResponse$Companion;", "", "()V", "fromJson", "", "json", "Lorg/json/JSONObject;", "onError", "throwable", "", "app_develDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String onError(@org.jetbrains.annotations.NotNull()
        java.lang.Throwable throwable) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String fromJson(@org.jetbrains.annotations.NotNull()
        org.json.JSONObject json) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}