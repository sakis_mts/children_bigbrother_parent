package com.mts.childrenbigbrother.adapter;

import java.lang.System;

/**
 * * Created by sakis on 3/18/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\"B5\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0018\u0010\b\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0002\u0010\fJ\u000e\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0007J\u0014\u0010\u0015\u001a\u00020\u000b2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00070\u0017J\b\u0010\u0018\u001a\u00020\nH\u0016J\u001a\u0010\u0019\u001a\u00020\u000b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u001b\u001a\u00020\nH\u0016J\u0018\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\nH\u0016J\u0014\u0010 \u001a\u00020\u000b2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0017J\u000e\u0010!\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0007R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R \u0010\b\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/mts/childrenbigbrother/adapter/ChildListAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "context", "Landroid/content/Context;", "arrChildren", "", "Lcom/mts/childrenbigbrother/model/Child;", "objListener", "Lkotlin/Function2;", "", "", "(Landroid/content/Context;Ljava/util/List;Lkotlin/jvm/functions/Function2;)V", "getArrChildren", "()Ljava/util/List;", "setArrChildren", "(Ljava/util/List;)V", "getContext", "()Landroid/content/Context;", "addChild", "child", "filterData", "arrUsers", "", "getItemCount", "onBindViewHolder", "objViewHolder", "i", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "refreshData", "removeChild", "UserViewHolder", "app_develDebug"})
public final class ChildListAdapter extends android.support.v7.widget.RecyclerView.Adapter<android.support.v7.widget.RecyclerView.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.mts.childrenbigbrother.model.Child> arrChildren;
    private final kotlin.jvm.functions.Function2<com.mts.childrenbigbrother.model.Child, java.lang.Integer, kotlin.Unit> objListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.support.v7.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.Nullable()
    android.support.v7.widget.RecyclerView.ViewHolder objViewHolder, int i) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void addChild(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.model.Child child) {
    }
    
    public final void removeChild(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.model.Child child) {
    }
    
    public final void refreshData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.mts.childrenbigbrother.model.Child> arrChildren) {
    }
    
    public final void filterData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.mts.childrenbigbrother.model.Child> arrUsers) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mts.childrenbigbrother.model.Child> getArrChildren() {
        return null;
    }
    
    public final void setArrChildren(@org.jetbrains.annotations.NotNull()
    java.util.List<com.mts.childrenbigbrother.model.Child> p0) {
    }
    
    public ChildListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mts.childrenbigbrother.model.Child> arrChildren, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super com.mts.childrenbigbrother.model.Child, ? super java.lang.Integer, kotlin.Unit> objListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J0\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0018\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000b0\u0011R\u001c\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/mts/childrenbigbrother/adapter/ChildListAdapter$UserViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "binding", "error/NonExistentClass", "(Lcom/mts/childrenbigbrother/adapter/ChildListAdapter;Lerror/NonExistentClass;)V", "getBinding", "()Lerror/NonExistentClass;", "setBinding", "(Lerror/NonExistentClass;)V", "Lerror/NonExistentClass;", "bindData", "", "index", "", "objChild", "Lcom/mts/childrenbigbrother/model/Child;", "objListener", "Lkotlin/Function2;", "app_develDebug"})
    public final class UserViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private error.NonExistentClass binding;
        
        public final void bindData(int index, @org.jetbrains.annotations.NotNull()
        com.mts.childrenbigbrother.model.Child objChild, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function2<? super com.mts.childrenbigbrother.model.Child, ? super java.lang.Integer, kotlin.Unit> objListener) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final error.NonExistentClass getBinding() {
            return null;
        }
        
        public final void setBinding(@org.jetbrains.annotations.NotNull()
        error.NonExistentClass p0) {
        }
        
        public UserViewHolder(@org.jetbrains.annotations.NotNull()
        error.NonExistentClass binding) {
            super(null);
        }
    }
}