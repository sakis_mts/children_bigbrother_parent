package com.mts.childrenbigbrother.model;

import java.lang.System;

/**
 * * Created by sakis on 2/10/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010)\u001a\b\u0012\u0004\u0012\u00020+0*2\u0006\u0010,\u001a\u00020\u001b2\u0006\u0010-\u001a\u00020\u001bJ\u0016\u0010.\u001a\u00020/2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u00100\u001a\u00020/J\u0006\u00101\u001a\u00020/J\u0006\u00102\u001a\u00020/J\u0006\u00103\u001a\u00020/R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R(\u0010\u0019\u001a\u0010\u0012\f\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b0\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R(\u0010!\u001a\u0010\u0012\f\u0012\n \u001c*\u0004\u0018\u00010\"0\"0\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001e\"\u0004\b$\u0010 R(\u0010%\u001a\u0010\u0012\f\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b0\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u001e\"\u0004\b\'\u0010 R\u000e\u0010(\u001a\u00020\u001bX\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u00064"}, d2 = {"Lcom/mts/childrenbigbrother/model/LoginViewModel;", "Landroid/arch/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "activity", "Landroid/app/Activity;", "getActivity", "()Landroid/app/Activity;", "setActivity", "(Landroid/app/Activity;)V", "compositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "fragment", "Landroid/support/v4/app/Fragment;", "getFragment", "()Landroid/support/v4/app/Fragment;", "setFragment", "(Landroid/support/v4/app/Fragment;)V", "loginService", "Lcom/sonin/soninandroidcommonlib/networking/OAuthService;", "getLoginService", "()Lcom/sonin/soninandroidcommonlib/networking/OAuthService;", "setLoginService", "(Lcom/sonin/soninandroidcommonlib/networking/OAuthService;)V", "obsPassword", "Landroid/databinding/ObservableField;", "", "kotlin.jvm.PlatformType", "getObsPassword", "()Landroid/databinding/ObservableField;", "setObsPassword", "(Landroid/databinding/ObservableField;)V", "obsProgrBar", "", "getObsProgrBar", "setObsProgrBar", "obsUsername", "getObsUsername", "setObsUsername", "strGrandType", "getAccessToken", "Lio/reactivex/Observable;", "Lcom/sonin/soninandroidcommonlib/models/AccessToken;", "username", "password", "initViewModel", "", "loginRequest", "onDestroy", "onPause", "onResume", "app_develDebug"})
public final class LoginViewModel extends android.arch.lifecycle.AndroidViewModel {
    @org.jetbrains.annotations.NotNull()
    public android.app.Activity activity;
    @org.jetbrains.annotations.NotNull()
    public android.support.v4.app.Fragment fragment;
    private final java.lang.String strGrandType = "password";
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.Integer> obsProgrBar;
    private io.reactivex.disposables.CompositeDisposable compositeDisposable;
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.String> obsUsername;
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.String> obsPassword;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.sonin.soninandroidcommonlib.networking.OAuthService loginService;
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Activity getActivity() {
        return null;
    }
    
    public final void setActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.v4.app.Fragment getFragment() {
        return null;
    }
    
    public final void setFragment(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.Integer> getObsProgrBar() {
        return null;
    }
    
    public final void setObsProgrBar(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.String> getObsUsername() {
        return null;
    }
    
    public final void setObsUsername(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.String> getObsPassword() {
        return null;
    }
    
    public final void setObsPassword(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.sonin.soninandroidcommonlib.networking.OAuthService getLoginService() {
        return null;
    }
    
    public final void setLoginService(@org.jetbrains.annotations.NotNull()
    com.sonin.soninandroidcommonlib.networking.OAuthService p0) {
    }
    
    public final void initViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment fragment) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<com.sonin.soninandroidcommonlib.models.AccessToken> getAccessToken(@org.jetbrains.annotations.NotNull()
    java.lang.String username, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
        return null;
    }
    
    public final void loginRequest() {
    }
    
    public final void onResume() {
    }
    
    public final void onDestroy() {
    }
    
    public final void onPause() {
    }
    
    public LoginViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}