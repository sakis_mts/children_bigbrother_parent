package com.mts.childrenbigbrother.view;

import java.lang.System;

/**
 * * Created by sakis on 3/18/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 ;2\u00020\u0001:\u0001;B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010.\u001a\u00020\tJ\u000e\u0010/\u001a\u00020\t2\u0006\u00100\u001a\u000201J\u0014\u00102\u001a\u00020\t2\n\b\u0001\u00103\u001a\u0004\u0018\u000104H\u0016J$\u00105\u001a\u0002012\u0006\u00106\u001a\u0002072\b\u00108\u001a\u0004\u0018\u0001092\b\u00103\u001a\u0004\u0018\u000104H\u0016J\b\u0010:\u001a\u00020\tH\u0016R7\u0010\u0003\u001a\u001f\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u0006\u0012\b\b\u0007\u0012\u0004\b\b(\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u00020\u000fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0014\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\u00020\u001aX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020\u001aX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u001c\"\u0004\b!\u0010\u001eR\u001a\u0010\"\u001a\u00020#X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'R\u001a\u0010(\u001a\u00020)X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-\u00a8\u0006<"}, d2 = {"Lcom/mts/childrenbigbrother/view/DialogFragmentAddChild;", "Landroid/support/v4/app/DialogFragment;", "()V", "callbackSave", "Lkotlin/Function1;", "Lcom/mts/childrenbigbrother/model/Child;", "Lkotlin/ParameterName;", "name", "child", "", "getCallbackSave", "()Lkotlin/jvm/functions/Function1;", "setCallbackSave", "(Lkotlin/jvm/functions/Function1;)V", "etName", "Landroid/widget/EditText;", "getEtName", "()Landroid/widget/EditText;", "setEtName", "(Landroid/widget/EditText;)V", "objNewChild", "getObjNewChild", "()Lcom/mts/childrenbigbrother/model/Child;", "setObjNewChild", "(Lcom/mts/childrenbigbrother/model/Child;)V", "radio_female", "Landroid/widget/RadioButton;", "getRadio_female", "()Landroid/widget/RadioButton;", "setRadio_female", "(Landroid/widget/RadioButton;)V", "radio_male", "getRadio_male", "setRadio_male", "rgGender", "Landroid/widget/RadioGroup;", "getRgGender", "()Landroid/widget/RadioGroup;", "setRgGender", "(Landroid/widget/RadioGroup;)V", "spinner", "Landroid/widget/Spinner;", "getSpinner", "()Landroid/widget/Spinner;", "setSpinner", "(Landroid/widget/Spinner;)V", "generateChild", "initVars", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onPause", "Companion", "app_develDebug"})
public final class DialogFragmentGeneric extends android.support.v4.app.DialogFragment {
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function1<? super com.mts.childrenbigbrother.model.Child, kotlin.Unit> callbackSave;
    @org.jetbrains.annotations.Nullable()
    private com.mts.childrenbigbrother.model.Child objNewChild;
    @org.jetbrains.annotations.NotNull()
    public android.widget.Spinner spinner;
    @org.jetbrains.annotations.NotNull()
    public android.widget.EditText etName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioGroup rgGender;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton radio_male;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton radio_female;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String BDK_BTN_SAVE_CALLBACK = "yes_callback";
    public static final com.mts.childrenbigbrother.view.DialogFragmentGeneric.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function1<com.mts.childrenbigbrother.model.Child, kotlin.Unit> getCallbackSave() {
        return null;
    }
    
    public final void setCallbackSave(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function1<? super com.mts.childrenbigbrother.model.Child, kotlin.Unit> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.mts.childrenbigbrother.model.Child getObjNewChild() {
        return null;
    }
    
    public final void setObjNewChild(@org.jetbrains.annotations.Nullable()
    com.mts.childrenbigbrother.model.Child p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.Spinner getSpinner() {
        return null;
    }
    
    public final void setSpinner(@org.jetbrains.annotations.NotNull()
    android.widget.Spinner p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.EditText getEtName() {
        return null;
    }
    
    public final void setEtName(@org.jetbrains.annotations.NotNull()
    android.widget.EditText p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioGroup getRgGender() {
        return null;
    }
    
    public final void setRgGender(@org.jetbrains.annotations.NotNull()
    android.widget.RadioGroup p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRadio_male() {
        return null;
    }
    
    public final void setRadio_male(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRadio_female() {
        return null;
    }
    
    public final void setRadio_female(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    @android.support.annotation.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public final void initVars(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void generateChild() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    public DialogFragmentGeneric() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J+\u0010\u0007\u001a\u00020\b2#\u0010\t\u001a\u001f\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\nR\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/mts/childrenbigbrother/view/DialogFragmentAddChild$Companion;", "", "()V", "BDK_BTN_SAVE_CALLBACK", "", "getBDK_BTN_SAVE_CALLBACK", "()Ljava/lang/String;", "newInstance", "Landroid/support/v4/app/DialogFragment;", "onSaveClicked", "Lkotlin/Function1;", "Lcom/mts/childrenbigbrother/model/Child;", "Lkotlin/ParameterName;", "name", "child", "", "app_develDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getBDK_BTN_SAVE_CALLBACK() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.support.v4.app.DialogFragment newInstance(@org.jetbrains.annotations.Nullable()
        kotlin.jvm.functions.Function1<? super com.mts.childrenbigbrother.model.Child, kotlin.Unit> onSaveClicked) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}