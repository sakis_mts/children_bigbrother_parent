package com.mts.childrenbigbrother;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0001(B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0011\u001a\u00020\u0012H\u0002J\u0006\u0010\u0013\u001a\u00020\u0012J\b\u0010\u0014\u001a\u00020\u0012H\u0002J\u0006\u0010\u0015\u001a\u00020\u0012J\u0006\u0010\u0016\u001a\u00020\u0012J\b\u0010\u0017\u001a\u00020\u0012H\u0016J\u0012\u0010\u0018\u001a\u00020\u00122\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0016J\u0012\u0010#\u001a\u00020\u00122\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0012\u0010&\u001a\u00020\u00122\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0012\u0010\'\u001a\u00020\u00122\b\u0010$\u001a\u0004\u0018\u00010%H\u0016R\u001e\u0010\u0005\u001a\u00060\u0006R\u00020\u0000X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006)"}, d2 = {"Lcom/mts/childrenbigbrother/NavigationActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;", "Landroid/support/design/widget/TabLayout$OnTabSelectedListener;", "()V", "adapter", "Lcom/mts/childrenbigbrother/NavigationActivity$PagerAdapter;", "getAdapter", "()Lcom/mts/childrenbigbrother/NavigationActivity$PagerAdapter;", "setAdapter", "(Lcom/mts/childrenbigbrother/NavigationActivity$PagerAdapter;)V", "toggle", "Landroid/support/v7/app/ActionBarDrawerToggle;", "getToggle", "()Landroid/support/v7/app/ActionBarDrawerToggle;", "setToggle", "(Landroid/support/v7/app/ActionBarDrawerToggle;)V", "changeTabsFont", "", "initDrawer", "initTablayout", "initToolbar", "initVars", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onNavigationItemSelected", "item", "Landroid/view/MenuItem;", "onOptionsItemSelected", "onTabReselected", "tab", "Landroid/support/design/widget/TabLayout$Tab;", "onTabSelected", "onTabUnselected", "PagerAdapter", "app_develDebug"})
public final class NavigationActivity extends android.support.v7.app.AppCompatActivity implements android.support.design.widget.NavigationView.OnNavigationItemSelectedListener, android.support.design.widget.TabLayout.OnTabSelectedListener {
    @org.jetbrains.annotations.NotNull()
    public com.mts.childrenbigbrother.NavigationActivity.PagerAdapter adapter;
    @org.jetbrains.annotations.NotNull()
    public android.support.v7.app.ActionBarDrawerToggle toggle;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.mts.childrenbigbrother.NavigationActivity.PagerAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.NavigationActivity.PagerAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.v7.app.ActionBarDrawerToggle getToggle() {
        return null;
    }
    
    public final void setToggle(@org.jetbrains.annotations.NotNull()
    android.support.v7.app.ActionBarDrawerToggle p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initDrawer() {
    }
    
    public final void initToolbar() {
    }
    
    public final void initVars() {
    }
    
    private final void initTablayout() {
    }
    
    private final void changeTabsFont() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onNavigationItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onTabReselected(@org.jetbrains.annotations.Nullable()
    android.support.design.widget.TabLayout.Tab tab) {
    }
    
    @java.lang.Override()
    public void onTabUnselected(@org.jetbrains.annotations.Nullable()
    android.support.design.widget.TabLayout.Tab tab) {
    }
    
    @java.lang.Override()
    public void onTabSelected(@org.jetbrains.annotations.Nullable()
    android.support.design.widget.TabLayout.Tab tab) {
    }
    
    public NavigationActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u000b\u001a\u00020\u0005H\u0016J\u0012\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u0005H\u0016R\u001a\u0010\u0004\u001a\u00020\u0005X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u000f"}, d2 = {"Lcom/mts/childrenbigbrother/NavigationActivity$PagerAdapter;", "Landroid/support/v4/app/FragmentStatePagerAdapter;", "fm", "Landroid/support/v4/app/FragmentManager;", "mNumOfTabs", "", "(Lcom/mts/childrenbigbrother/NavigationActivity;Landroid/support/v4/app/FragmentManager;I)V", "getMNumOfTabs$app_develDebug", "()I", "setMNumOfTabs$app_develDebug", "(I)V", "getCount", "getItem", "Landroid/support/v4/app/Fragment;", "position", "app_develDebug"})
    public final class PagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {
        private int mNumOfTabs;
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public android.support.v4.app.Fragment getItem(int position) {
            return null;
        }
        
        @java.lang.Override()
        public int getCount() {
            return 0;
        }
        
        public final int getMNumOfTabs$app_develDebug() {
            return 0;
        }
        
        public final void setMNumOfTabs$app_develDebug(int p0) {
        }
        
        public PagerAdapter(@org.jetbrains.annotations.NotNull()
        android.support.v4.app.FragmentManager fm, int mNumOfTabs) {
            super(null);
        }
    }
}