package com.mts.childrenbigbrother.viewmodel;

import java.lang.System;

/**
 * * Created by sakis on 3/18/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\tR \u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u0007\"\u0004\b\u000f\u0010\t\u00a8\u0006\u0010"}, d2 = {"Lcom/mts/childrenbigbrother/viewmodel/ChildViewModel;", "", "()V", "strChildGenter", "Landroid/databinding/ObservableField;", "", "getStrChildGenter", "()Landroid/databinding/ObservableField;", "setStrChildGenter", "(Landroid/databinding/ObservableField;)V", "strChildName", "getStrChildName", "setStrChildName", "strChildeAge", "getStrChildeAge", "setStrChildeAge", "app_develDebug"})
public final class ChildViewModel {
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.String> strChildName;
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.String> strChildeAge;
    @org.jetbrains.annotations.NotNull()
    private android.databinding.ObservableField<java.lang.String> strChildGenter;
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.String> getStrChildName() {
        return null;
    }
    
    public final void setStrChildName(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.String> getStrChildeAge() {
        return null;
    }
    
    public final void setStrChildeAge(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.databinding.ObservableField<java.lang.String> getStrChildGenter() {
        return null;
    }
    
    public final void setStrChildGenter(@org.jetbrains.annotations.NotNull()
    android.databinding.ObservableField<java.lang.String> p0) {
    }
    
    public ChildViewModel() {
        super();
    }
}