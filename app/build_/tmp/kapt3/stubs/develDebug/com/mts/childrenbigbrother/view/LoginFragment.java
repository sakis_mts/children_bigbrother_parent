package com.mts.childrenbigbrother.view;

import java.lang.System;

/**
 * * Created by sakis on 2/10/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0010\u001a\u00020\u000fH\u0016J\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0012J\u0012\u0010\u0014\u001a\u00020\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J(\u0010\u001a\u001a\u0004\u0018\u00010\u00192\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u001c\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/mts/childrenbigbrother/view/LoginFragment;", "Landroid/support/v4/app/Fragment;", "Landroid/arch/lifecycle/LifecycleRegistryOwner;", "Landroid/view/View$OnClickListener;", "()V", "loginFragmentBinding", "error/NonExistentClass", "getLoginFragmentBinding", "()Lerror/NonExistentClass;", "setLoginFragmentBinding", "(Lerror/NonExistentClass;)V", "Lerror/NonExistentClass;", "loginViewModel", "Lcom/mts/childrenbigbrother/model/LoginViewModel;", "mRegistry", "Landroid/arch/lifecycle/LifecycleRegistry;", "getLifecycle", "initVars", "", "initViewModel", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onClick", "v", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "app_develDebug"})
public final class LoginFragment extends android.support.v4.app.Fragment implements android.arch.lifecycle.LifecycleRegistryOwner, android.view.View.OnClickListener {
    private final android.arch.lifecycle.LifecycleRegistry mRegistry = null;
    private com.mts.childrenbigbrother.model.LoginViewModel loginViewModel;
    @org.jetbrains.annotations.NotNull()
    public error.NonExistentClass loginFragmentBinding;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.arch.lifecycle.LifecycleRegistry getLifecycle() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final error.NonExistentClass getLoginFragmentBinding() {
        return null;
    }
    
    public final void setLoginFragmentBinding(@org.jetbrains.annotations.NotNull()
    error.NonExistentClass p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.Nullable()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initViewModel() {
    }
    
    public final void initVars() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public LoginFragment() {
        super();
    }
}