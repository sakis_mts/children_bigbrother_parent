package com.sonin.soninandroidblankproject.networking;

import java.lang.System;

/**
 * * Created by athanasios.moutsioul on 19/10/2017.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'\u00a8\u0006\u0005"}, d2 = {"Lcom/sonin/soninandroidblankproject/networking/NetworkRequests;", "", "getUser", "Lio/reactivex/Single;", "error/NonExistentClass", "app_develDebug"})
public abstract interface NetworkRequests {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "api/customer/me")
    public abstract io.reactivex.Single<error.NonExistentClass> getUser();
}