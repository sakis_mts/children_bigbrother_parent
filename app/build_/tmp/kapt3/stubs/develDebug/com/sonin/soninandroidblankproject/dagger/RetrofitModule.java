package com.sonin.soninandroidblankproject.dagger;

import java.lang.System;

/**
 * * Created by athanasios.moutsioul on 22/02/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0007J\u0012\u0010\u001b\u001a\u00020\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001eH\u0007J\u0012\u0010\u001f\u001a\u00020 2\b\b\u0001\u0010\u001d\u001a\u00020\u001eH\u0007J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0018H\u0007J\b\u0010$\u001a\u00020\"H\u0007J\u0012\u0010%\u001a\u00020\u001e2\b\b\u0001\u0010&\u001a\u00020\"H\u0007J\u0012\u0010\'\u001a\u00020\u001e2\b\b\u0001\u0010&\u001a\u00020\"H\u0007R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006("}, d2 = {"Lcom/sonin/soninandroidblankproject/dagger/RetrofitModule;", "", "strEndpoint", "", "timeout", "", "accessToken", "strUserName", "accountType", "converter", "Lretrofit2/Converter$Factory;", "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lretrofit2/Converter$Factory;)V", "getAccessToken", "()Ljava/lang/String;", "getAccountType", "getConverter", "()Lretrofit2/Converter$Factory;", "setConverter", "(Lretrofit2/Converter$Factory;)V", "getStrEndpoint", "getStrUserName", "getTimeout", "()J", "provideAccountManager", "Landroid/accounts/AccountManager;", "context", "Landroid/content/Context;", "provideItemServiceWithAccessToken", "Lcom/sonin/soninandroidblankproject/networking/NetworkRequests;", "retrofit", "Lretrofit2/Retrofit;", "provideItemServiceWithoutAccessToken", "Lcom/sonin/soninandroidcommonlib/networking/OAuthService;", "provideOkhttpClientWithAccessToken", "Lokhttp3/OkHttpClient;", "accountManager", "provideOkhttpClientWithoutAccessToken", "provideRetrofitWithAccessToken", "okHttpClient", "provideRetrofitWithoutAccessToken", "app_develDebug"})
@dagger.Module()
public final class RetrofitModule {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String strEndpoint = null;
    private final long timeout = 0L;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String accessToken = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String strUserName = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String accountType = null;
    @org.jetbrains.annotations.Nullable()
    private retrofit2.Converter.Factory converter;
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "WithoutAccessToken")
    @javax.inject.Singleton()
    @dagger.Provides()
    public final okhttp3.OkHttpClient provideOkhttpClientWithoutAccessToken() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "WithAccessToken")
    @javax.inject.Singleton()
    @dagger.Provides()
    public final okhttp3.OkHttpClient provideOkhttpClientWithAccessToken(@org.jetbrains.annotations.NotNull()
    android.accounts.AccountManager accountManager) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final android.accounts.AccountManager provideAccountManager(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "RetrofitWithoutAccessToken")
    @javax.inject.Singleton()
    @dagger.Provides()
    public final retrofit2.Retrofit provideRetrofitWithoutAccessToken(@org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "WithoutAccessToken")
    okhttp3.OkHttpClient okHttpClient) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "RetrofitWithAccessToken")
    @javax.inject.Singleton()
    @dagger.Provides()
    public final retrofit2.Retrofit provideRetrofitWithAccessToken(@org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "WithAccessToken")
    okhttp3.OkHttpClient okHttpClient) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.sonin.soninandroidcommonlib.networking.OAuthService provideItemServiceWithoutAccessToken(@org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "RetrofitWithoutAccessToken")
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.sonin.soninandroidblankproject.networking.NetworkRequests provideItemServiceWithAccessToken(@org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "RetrofitWithAccessToken")
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStrEndpoint() {
        return null;
    }
    
    public final long getTimeout() {
        return 0L;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccessToken() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStrUserName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountType() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final retrofit2.Converter.Factory getConverter() {
        return null;
    }
    
    public final void setConverter(@org.jetbrains.annotations.Nullable()
    retrofit2.Converter.Factory p0) {
    }
    
    public RetrofitModule(@org.jetbrains.annotations.NotNull()
    java.lang.String strEndpoint, long timeout, @org.jetbrains.annotations.Nullable()
    java.lang.String accessToken, @org.jetbrains.annotations.Nullable()
    java.lang.String strUserName, @org.jetbrains.annotations.Nullable()
    java.lang.String accountType, @org.jetbrains.annotations.Nullable()
    retrofit2.Converter.Factory converter) {
        super();
    }
}