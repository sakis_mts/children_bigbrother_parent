package com.sonin.bullittracker.networking;

import java.lang.System;

/**
 * * Created by athanasios.moutsioul on 04/01/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/sonin/bullittracker/networking/NULL_TO_EMPTY_STRING_ADAPTER;", "", "()V", "fromJson", "", "reader", "Lcom/squareup/moshi/JsonReader;", "app_develDebug"})
public final class NULL_TO_EMPTY_STRING_ADAPTER {
    public static final com.sonin.bullittracker.networking.NULL_TO_EMPTY_STRING_ADAPTER INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.FromJson()
    public final java.lang.String fromJson(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) {
        return null;
    }
    
    private NULL_TO_EMPTY_STRING_ADAPTER() {
        super();
    }
}