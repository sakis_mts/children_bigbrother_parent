package com.sonin.soninandroidblankproject.dagger;

import java.lang.System;

/**
 * * Created by athanasios.moutsioul on 23/02/2018.
 */
@kotlin.Metadata(mv = {1, 1, 9}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/sonin/soninandroidblankproject/dagger/AppComponent;", "", "inject", "", "target", "Lcom/mts/childrenbigbrother/model/LoginViewModel;", "app_develDebug"})
@dagger.Component(modules = {com.sonin.soninandroidblankproject.dagger.AppModule.class, com.sonin.soninandroidblankproject.dagger.RetrofitModule.class})
@javax.inject.Singleton()
public abstract interface AppComponent {
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.mts.childrenbigbrother.model.LoginViewModel target);
}