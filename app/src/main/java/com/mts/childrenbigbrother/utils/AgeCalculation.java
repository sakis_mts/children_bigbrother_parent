package com.mts.childrenbigbrother.utils;

import java.util.Calendar;

public class AgeCalculation {
    private int startYear;
    private int startMonth;
    private int startDay;
    private int endYear;
    private int endMonth;
    private int endDay;
    private int resYear;
    private int resMonth;
    private int resDay;
    public String getCurrentDate()
    {
        Calendar c=Calendar.getInstance();
        endYear=c.get(Calendar.YEAR);
        endMonth=c.get(Calendar.MONTH);
        endMonth++;
        endDay=c.get(Calendar.DAY_OF_MONTH);
        return endDay+":"+endMonth+":"+endYear;
    }
    public void setDateOfBirth(int sYear, int sMonth, int sDay)
    {
        startYear=sYear;
        startMonth=sMonth;
        startMonth++;
        startDay=sDay;

    }
    public void calculateYear()
    {
        resYear=endYear-startYear;

    }

    public void calculateMonth()
    {
        if(endMonth>=startMonth)
        {
            resMonth= endMonth-startMonth;
        }
        else
        {
            resMonth=endMonth-startMonth;
            resMonth=12+resMonth;
            resYear--;
        }

    }
    public void calculateDay()
    {

        if(endDay>=startDay)
        {
            resDay= endDay-startDay;
        }
        else
        {
            resDay=endDay-startDay;
            resDay=30+resDay;
//resMonth--;
        }
    }

    public String getResult(int sYear, int sMonth, int sDay)
    {
        getCurrentDate();
        setDateOfBirth(sYear,sMonth,sDay);
        calculateYear();
        calculateMonth();
        calculateDay();

        String strMonth =resMonth +" months and " ;
        if (resMonth== 1){

            strMonth = resMonth +" month and " ;
        }
        String result = resYear+ " years, "+strMonth+resDay+" days";
        return result;
    }

}