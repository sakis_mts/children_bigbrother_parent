package com.mts.childrenbigbrother.utils

import java.text.SimpleDateFormat
import java.util.*



fun Long.getDateTime(format: String? = null): String? {
    try {
        val sdf = SimpleDateFormat(format ?: "dd MMM yyyy HH:mm")
        val netDate = Date(this*1000L)
        return sdf.format(netDate)
    } catch (e: Throwable) {
        return e.toString()
    }
}

fun getAge(strDate: String): String{

    try {
        val sdf = SimpleDateFormat( "dd/MM/yyyy")
        val date = sdf.parse(strDate) as Date
        val cal = Calendar.getInstance()
        cal.time = date

        val age = AgeCalculation().getResult(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))

        return age
    } catch (e: Throwable) {
        return e.toString()
    }

}