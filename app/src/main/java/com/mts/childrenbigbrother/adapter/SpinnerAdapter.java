package com.mts.childrenbigbrother.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by sakis on 3/18/2018.
 */

public class SpinnerAdapter extends ArrayAdapter<String>
{
    private int intFontSize;
    private Context context;

    public SpinnerAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView         = super.getView(position, convertView, parent);
//        Typeface typeface   = ResourcesCompat.getFont(context, R.font.avenir_book);
//
//        ((TextView) convertView).setTypeface(typeface);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);

//        Typeface typeface = ResourcesCompat.getFont(context, R.font.avenir_book);
//        ((TextView) convertView).setTypeface(typeface);


        return convertView;
    }
}
