package com.mts.childrenbigbrother.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.databinding.ListitemChildBinding
import com.mts.childrenbigbrother.databinding.ListitemNotificationBinding
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.Notification
import com.mts.childrenbigbrother.utils.getDateTime
import com.mts.childrenbigbrother.viewmodel.ChildViewModel
import com.mts.childrenbigbrother.viewmodel.NotificationViewModel
import org.jetbrains.anko.backgroundResource
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.R.attr.mode
import android.graphics.Color
import android.graphics.Color.LTGRAY
import android.support.v4.content.ContextCompat
import android.widget.RelativeLayout
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.R.id.frameLayout
import com.mts.childrenbigbrother.database.AppDatabase
import com.mts.childrenbigbrother.utils.CircleTransform
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.content_navigation.*
import org.jetbrains.anko.backgroundColor
import java.io.File


/**
 * Created by sakis on 5/27/2018.
 */
class NotificationAdapter (val context: Context, var arrNotification:MutableList<Notification>,var arrChildren: MutableList<Child>, private val objListener: (Notification, Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    private var multiSelect                 = false
    private val selectedItems               = ArrayList<Notification>()
    private var blnActionModeEnabled        = false
    private  var mActionMode : ActionMode?  = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
    {

        val inflater                                = parent!!.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding: ListitemNotificationBinding    = DataBindingUtil.inflate(inflater, R.layout.listitem_notification, parent, false)
        binding.notificationViewModel               = NotificationViewModel()

        return NotificationViewHolder(binding)

    }

    override fun onBindViewHolder(objViewHolder: RecyclerView.ViewHolder?, i: Int)
    {
        (objViewHolder as NotificationViewHolder)?.bindData(i, arrNotification[i], objListener)

    }
    override fun getItemCount(): Int {
        return arrNotification.size
    }

    fun addChild(notidication: Notification){

        arrNotification.add(notidication)
        notifyDataSetChanged()

    }

    fun removeChild(notidication: Notification){

        if (!arrNotification.isEmpty()){

            arrNotification.remove(notidication)

        }

    }

    fun refreshData(arrNotification: List<Notification>){

        if (!arrNotification.isEmpty()){

            this.arrNotification.clear()
            this.arrNotification.addAll(arrNotification)
            this.arrNotification.sortByDescending { it.dateCreated }
            notifyDataSetChanged()
        }

    }

    fun refreshChildrenData(arrChildren: MutableList<Child>){

        if (!arrChildren.isEmpty()){

            this.arrChildren.clear()
            this.arrChildren.addAll(arrChildren)
            notifyDataSetChanged()
        }

    }

    fun filterData(arrNotification: List<Notification>){

        this.arrNotification.clear()
        this.arrNotification.addAll(arrNotification)
        notifyDataSetChanged()
    }


    inner class NotificationViewHolder(var binding: ListitemNotificationBinding) : RecyclerView.ViewHolder(binding.root)
    {

        fun bindData(index: Int, objNotification: Notification, objListener: (Notification, Int) -> Unit) {

            if (objNotification.notificationType == Notification.OUTGOING){

                binding.tvFromTo.text ="To"
                binding.ivType.setImageResource(R.drawable.ic_outgoing_not)
                binding.ivType.setColorFilter(ContextCompat.getColor(context, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY)

            }else{

                binding.tvFromTo.text ="From"
                binding.ivType.setImageResource(R.drawable.ic_incoming_not)
                binding.ivType.setColorFilter(ContextCompat.getColor(context, R.color.green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            objNotification.blnFailed?.let {

                if (it){
                    binding.tvFailed.visibility = View.VISIBLE
                }else{
                    binding.tvFailed.visibility = View.GONE
                }

            }
            binding.rlRoot.backgroundColor = Color.WHITE
            binding.notificationViewModel?.notificationPerson?.set(objNotification.strChildName)

            binding.notificationViewModel?.strNotificationMessage?.set(objNotification.strMessage)
            objNotification.dateCreated?.let { binding.tvDate.text = it.getDateTime() }

            itemView.setOnClickListener {

                if (blnActionModeEnabled){
                    mActionMode?.finish()
                }

            }

            if (!arrChildren.isEmpty()){

               val objChild = arrChildren.first { objChild-> objChild.strName.equals(objNotification.strChildName)}

                objChild.imagePath?.let {imagepath->

                        if (!imagepath.isEmpty()){

                            val f = File(imagepath)
                            if (f.exists()){

                                Picasso.with(context).load(f).transform(CircleTransform()).into(binding.ivChild)
                            }


                        }
                    }?:kotlin.run {

                        Picasso.with(context).load(R.drawable.ic_placeholder).into(binding.ivChild)
                    }


                }
            itemView.setOnLongClickListener {

                if (blnActionModeEnabled){

                    mActionMode?.finish()

                }else{

                    (context as NavigationActivity).toolbar.startActionMode(actionModeCallbacks)
                    selectItem(objNotification,it)

                }

                return@setOnLongClickListener true
            }


        }


        fun selectItem(item: Notification, view: View) {
            if (multiSelect) {
                if (selectedItems.contains(item)) {
                    selectedItems.remove(item)
                    view.findViewById<RelativeLayout>(R.id.rlRoot).backgroundColor = Color.WHITE
                } else {
                    selectedItems.add(item)
                    view.findViewById<RelativeLayout>(R.id.rlRoot).backgroundColor = Color.LTGRAY

                }
            }
        }

    }

    public val actionModeCallbacks : ActionMode.Callback = object : ActionMode.Callback {
        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            for (objItem in selectedItems) {
                arrNotification.remove(objItem)
                objItem.deleteItem(AppDatabase.getDatabase(context))
            }
            mode.finish()
            return true
        }

        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            multiSelect = true;
            menu.add("Delete");
            blnActionModeEnabled = true
            mActionMode = mode
            return true;
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            return false
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            blnActionModeEnabled = false
            multiSelect = false;
            selectedItems.clear();
            notifyDataSetChanged();
            mActionMode = null

        }

    }

}