package com.mts.childrenbigbrother.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.databinding.ListitemChildBinding
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.utils.CircleTransform
import com.mts.childrenbigbrother.utils.getAge
import com.mts.childrenbigbrother.viewmodel.ChildViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.listitem_child.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundResource
import retrofit2.adapter.rxjava2.Result.response
import java.io.File
import java.net.URI


/**
 * Created by sakis on 3/18/2018.
 */
class ChildListAdapter (val context: Context, var arrChildren:MutableList<Child>, private val objListener: (Child, Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
    {

        val inflater                                = parent!!.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding: ListitemChildBinding           = DataBindingUtil.inflate(inflater, R.layout.listitem_child, parent, false)
        binding.itemChildViewModel                  = ChildViewModel()

        return UserViewHolder(binding)

    }

    override fun onBindViewHolder(objViewHolder: RecyclerView.ViewHolder?, i: Int)
    {
        (objViewHolder as UserViewHolder)?.bindData(i, arrChildren[i], objListener)

    }
    override fun getItemCount(): Int {
        return arrChildren.size
    }

    fun addChild(child: Child){

        arrChildren.add(child)
        notifyDataSetChanged()

    }

    fun removeChild(child: Child){

        if (!arrChildren.isEmpty()){

            arrChildren.remove(child)

        }

    }

    fun refreshData(arrChildren: List<Child>){

        if (!arrChildren.isEmpty()){

            this.arrChildren.clear()
            this.arrChildren.addAll(arrChildren)
            notifyDataSetChanged()
        }

    }

    fun filterData(arrUsers: List<Child>){

        this.arrChildren.clear()
        this.arrChildren.addAll(arrUsers)
        notifyDataSetChanged()
    }


    inner class UserViewHolder(var binding: ListitemChildBinding) : RecyclerView.ViewHolder(binding.root)
    {

        fun bindData(index: Int, objChild: Child,  objListener: (Child, Int) -> Unit) {

            binding.itemChildViewModel?.strChildName?.set(objChild.strName)
            binding.itemChildViewModel?.strChildeAge?.set(getAge(objChild.strAge))
            binding.itemChildViewModel?.strCode?.set(objChild.codes)

            if (objChild.blnMale == 1){

                binding.itemChildViewModel?.strChildGenter?.set(context.getString(R.string.child_male))
                binding.bottomBorder .backgroundColor = ContextCompat.getColor(context, R.color.accent)
               // binding.rlRoot.backgroundResource = R.drawable.blue_child_bg
            }else{

                binding.itemChildViewModel?.strChildGenter?.set(context.getString(R.string.child_female))
                binding.bottomBorder.backgroundColor =  ContextCompat.getColor(context, R.color.primary)
               // binding.rlRoot.backgroundResource = R.drawable.pink_child_bg
            }
            objChild.imagePath?.let {imagepath->

                if (!imagepath.isEmpty()){

                    val f = File(imagepath)
                    if (f.exists()){

                        Picasso.with(context).load(f).transform(CircleTransform()).into(binding.ivPhoto)
                    }


                }
            }?:kotlin.run {

                Picasso.with(context).load(R.drawable.ic_placeholder).into(binding.ivPhoto)
            }

           binding.ivTrack.setOnClickListener {
               objListener(objChild,index)
           }
            itemView.setOnClickListener { }


        }

    }

}