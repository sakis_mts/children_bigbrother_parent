package com.sonin.soninandroidblankproject.networking


import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.Notification
import com.mts.childrenbigbrother.model.User
import dagger.internal.SingleCheck

import java.util.*
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by athanasios.moutsioul on 19/10/2017.
 */
interface NetworkRequests {

    @POST("api/createChild")
    fun createChild(@Body child: Child): Single<Child>

    @POST("api/getChildren")
    @FormUrlEncoded
    fun getChildren( @Field("parentId") parentId: Int): Observable<List<Child>>

    @POST("api/testPush")
    fun sendPush(@Body requestBody: RequestBody): Single<ResponseBody>

    @POST("api/sendPush")
    fun sendPush(@Body notification: Notification): Single<ResponseBody>

    @POST("api/registerFirebaseToken")
    fun registerFirebaseToken(@Body requestBody: RequestBody): Single<ResponseBody>

    @GET("api/getUser")
    fun getUser( ): Single<User>

    // Auth
    @FormUrlEncoded
    @POST("/api/register")
    fun registerParent(
            @Field("name") name: String,
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("phone") phone: String,
            @Field("c_password") c_password: String,
            @Field("address") address: String): Single<Response<ResponseBody>>

}