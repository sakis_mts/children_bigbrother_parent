package com.sonin.bullittracker.networking

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader

/**
 * Created by athanasios.moutsioul on 04/01/2018.
 */
object NULL_TO_EMPTY_STRING_ADAPTER {

    @FromJson
    fun fromJson(reader: JsonReader): String {
        if (reader.peek() != JsonReader.Token.NULL) {
            return reader.nextString()
        }
        reader.nextNull<Unit>()
        return ""
    }
}