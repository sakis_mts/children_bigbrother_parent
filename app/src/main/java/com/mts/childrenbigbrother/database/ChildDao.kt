package com.mts.childrenbigbrother.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.mts.childrenbigbrother.model.Child

/**
 * Created by athanasios.moutsioul on 11/04/2018.
 */
@Dao
interface ChildDao {

    @Query("select * from Children")
    fun getAllChildren(): LiveData<List<Child>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addSingleChild(child: Child)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addChidren(arrChildren: List<Child>)

    @Delete
    fun deleteChild(child: Child)

    @Query("DELETE FROM Children")
    fun deleteChildren()
}