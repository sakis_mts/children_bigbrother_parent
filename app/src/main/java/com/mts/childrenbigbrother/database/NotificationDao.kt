package com.mts.childrenbigbrother.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.Notification

/**
 * Created by sakis on 6/17/2018.
 */
@Dao
interface NotificationDao {

    @Query("select * from Notification ORDER BY dateCreated DESC")
    fun getAllNotification(): LiveData<List<Notification>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addSingleNotification(notification: Notification): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNotification(arrNotification: List<Notification>)

    @Delete
    fun deleteNotification(notification: Notification)
    @Update
    fun updateNotification(notification: Notification)

    @Query("DELETE FROM Notification")
    fun deleteNotification()
}