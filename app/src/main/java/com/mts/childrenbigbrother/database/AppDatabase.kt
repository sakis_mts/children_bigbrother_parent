package com.mts.childrenbigbrother.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.mts.childrenbigbrother.database.ChildLocationUpdatedDao
import com.mts.childrenbigbrother.database.NotificationDao
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.ChildLocationUpdates
import com.mts.childrenbigbrother.model.Notification
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidcommonlib.utils.TangerineLog

@Database(entities = arrayOf( Child::class, ChildLocationUpdates::class, Notification::class), version = 1)
abstract class AppDatabase : RoomDatabase() {



    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            println("Database name: " + SessionManager.instance.getLoggedInUsername())
//            INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "marmalade-database_" + SessionManager.instance.getLoggedInUsername())
//                    // allow queries on the main thread.
//                    // Don't do this on a real app! See PersistenceBasicSample for an example.
//                    .allowMainThreadQueries()
//                    .build()

            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        context,
                        AppDatabase::class.java,
                        "children_bb_db")
                        .build()
            }
            TangerineLog.log("dbInstance: ${INSTANCE}")
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

    abstract fun childModel()                       : ChildDao
    abstract fun transmitionLocationModel()         : ChildLocationUpdatedDao
    abstract fun notificationModel()                : NotificationDao


}