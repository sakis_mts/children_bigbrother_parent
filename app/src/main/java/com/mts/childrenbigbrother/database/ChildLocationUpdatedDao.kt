package com.mts.childrenbigbrother.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.mts.childrenbigbrother.model.ChildLocationUpdates


/**
 * Created by athanasios.moutsioul on 11/04/2018.
 */
@Dao
interface ChildLocationUpdatedDao {

    @Query("select * from ChildLocationUpdates")
    fun getAllLocationUpdates(): List<ChildLocationUpdates>

    @Query("select * from ChildLocationUpdates WHERE child_id IS :childId")
    fun getAllLocationUpdatesByChild(childId: Int): List<ChildLocationUpdates>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addSingleLocation(childLocationUpdates: ChildLocationUpdates)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addLocations(arrChildLocationUpdates: List<ChildLocationUpdates>)

    @Delete
    fun deleteLocation(childLocationUpdates: ChildLocationUpdates)

    @Query("DELETE FROM ChildLocationUpdates")
    fun deleteLocations()

}