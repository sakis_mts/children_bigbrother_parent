package com.mts.childrenbigbrother.viewmodel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.databinding.ObservableField
import android.support.v4.app.Fragment
import android.view.View
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.model.BaseCommand
import com.mts.childrenbigbrother.model.RetrofitErrorResponse
import com.mts.childrenbigbrother.utils.SingleLiveEvent
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidcommonlib.models.AccessToken
import com.sonin.soninandroidcommonlib.networking.OAuthService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import retrofit2.Response
import javax.inject.Inject

class RegisterViewModel(application: Application) : AndroidViewModel(application) {

    var obsProgrBar                 = ObservableField(View.GONE)
    var obsFName                    = ObservableField("athan")
    var obsLName                    = ObservableField("mts")
    var obsPassword                 = ObservableField("12345")
    var obsConfPass                 = ObservableField("12345")
    var obsPhone                    = ObservableField("234234")
    var obsEmail                    = ObservableField("athan10@gmail.com")
    var obsAddress                  = ObservableField("dfsdfsd")
    private var compositeDisposable = CompositeDisposable()
    private val strGrandType        = "password"
    val command                     : SingleLiveEvent<BaseCommand> = SingleLiveEvent()

    @Inject
    lateinit var networkService: NetworkRequests

    @Inject
    lateinit var oauthService: OAuthService

    @Inject
    lateinit  var context : Context

    init {
        ChildrenBigBrotherApp.instance?.retrofitComponent?.inject(this)
    }

    fun registerParent() : Single<Response<ResponseBody>> {

        return networkService.registerParent("${obsFName.get()} ${obsLName.get()}", obsEmail.get(), obsPassword.get(),obsPhone.get(), obsConfPass.get(),obsAddress.get())

    }

    fun getAccessToken(username: String, password: String) : Observable<AccessToken> {

        return oauthService.getAccessToken(strGrandType, username, password, context.getString(R.string.client_id), context.getString(R.string.client_secret))

    }

    fun registerParentRequest(){

        obsProgrBar.set(View.VISIBLE)
        compositeDisposable.add(
                registerParent()
                        .map {

                            val accessToken = getAccessToken(obsEmail.get(),obsPassword.get()).blockingFirst()

                            return@map accessToken
                        }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->

                            obsProgrBar.set(View.GONE)
                            SessionManager.instance.setAccount(context,result,context.getString(R.string.account_type),obsEmail.get(),true, true)
                            command.value = BaseCommand(true)
                            ChildrenBigBrotherApp.instance.updateDagger(SessionManager.instance.getAuthorizationAccessTokenType(),obsEmail.get(),context.getString(R.string.account_type))
                            println(SessionManager.instance.getAuthorizationAccessTokenType())

                        }, { error ->


                            RetrofitErrorResponse.onError(error)?.let {  command.value = BaseCommand(false, it) }
                            println(error)
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)

                        })


        )


    }

}