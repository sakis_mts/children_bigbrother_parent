package com.mts.childrenbigbrother.viewmodel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.databinding.ObservableField
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.View
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.mts.childrenbigbrother.BuildConfig
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp.Companion.BDK_USER
import com.mts.childrenbigbrother.database.AppDatabase
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.RetrofitErrorResponse
import com.mts.childrenbigbrother.model.User
import com.mts.childrenbigbrother.view.ChildrenListFragment
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidcommonlib.networking.OAuthService
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import javax.inject.Inject

/**
 * Created by sakis on 2/18/2018.
 */
class NavChildrenListViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var activity           : Activity
    lateinit var fragment           : Fragment
    var obsProgrBar                 = ObservableField(View.GONE)
    private var compositeDisposable = CompositeDisposable()
    val arrChildren                 : LiveData<List<Child>>
    val appDatabase                 : AppDatabase
    @Inject
    lateinit var apiService: NetworkRequests

    init {
        ChildrenBigBrotherApp.instance?.retrofitComponent?.inject(this)
        appDatabase     = AppDatabase.getDatabase(this.getApplication<Application>())
        arrChildren     = appDatabase.childModel().getAllChildren()
    }

    fun initViewModel(activity: Activity, fragment: Fragment){

        this.activity       = activity
        this.fragment       = fragment


    }
    fun createChildRequest(child: Child){

        obsProgrBar.set(View.VISIBLE)
        compositeDisposable.add(
                createChild(child)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->

                            obsProgrBar.set(View.GONE)
                            (fragment as ChildrenListFragment).addChild(result)
                            result.insertItem(appDatabase)


                        }, { error ->

                            RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)
                        })

        )

    }

    fun getChildrenRequest(parentId: Int){

        compositeDisposable.add(
                getChildren(parentId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->

                            obsProgrBar.set(View.GONE)
                            setChildren(result)
                            doAsync {
                                appDatabase.transmitionLocationModel().deleteLocations()
                                appDatabase.childModel().deleteChildren()
                                result.forEach { child ->

                                    appDatabase.childModel().addSingleChild(child)
                                    child.arrTransmition?.let { arrTransmition ->

                                        appDatabase.transmitionLocationModel().addLocations(arrTransmition)
                                    }

                                }
                            }

                        }, { error ->

                            RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)
                        })

        )

    }

    fun registerFirebaseTokenRequest(){
        createRequestBody()?.let {

            compositeDisposable.add(
                    registerFirebaseToken(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe ({
                                result ->

                                println("Firebase"+result)

                            }, { error ->
                                println(error)
                                RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                                error.printStackTrace()
                            })

            )

        }


    }

    fun createRequestBodyForPush(): RequestBody?{
        var prefs = PreferenceManager.getDefaultSharedPreferences(activity)

        val userId =  prefs.getInt("userId",-1)
        if (userId != -1) {

            var jsonParams = JSONObject()
            FirebaseInstanceId.getInstance()?.let {
                it.token?.let { token->

                    if (!token.isEmpty()){

                        jsonParams.put("userId", userId)

                        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(jsonParams).toString());
                    }
                }

            }
        }
        return null
    }

    fun createRequestBody(): RequestBody?{
        var prefs = PreferenceManager.getDefaultSharedPreferences(activity)

        val userId =  prefs.getInt("userId",-1)
        if (userId != -1) {

            var jsonParams = JSONObject()
            FirebaseInstanceId.getInstance()?.let {
                it.token?.let { token->

                    if (!token.isEmpty()){

                        jsonParams.put("token", token )
                        jsonParams.put("userId", userId)
                        jsonParams.put("userType","parent")
                        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(jsonParams).toString());
                    }
                }

            }
        }
        return null
    }

    fun testPushReqest(){

        createRequestBodyForPush()?.let {

            compositeDisposable.add(
                    testPush(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe ({
                                result ->

                                println(result)

                            }, { error ->
                                println(error)
                                RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                                error.printStackTrace()
                            })

            )

        }


    }

    fun getUserRequest(){
        obsProgrBar.set(View.VISIBLE)
        compositeDisposable.add(
                getUser()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->

                            var prefs = PreferenceManager.getDefaultSharedPreferences(activity)
                            prefs.edit().putString(BDK_USER, Gson().toJson(result)).commit()
                            getChildrenRequest(result.id)
                            (activity as NavigationActivity).updateNavHeaderInfo(result)
                            if (prefs.getInt("userId",-1)!= result.id ){
                                prefs.edit().putInt("userId", result.id).commit()

                                registerFirebaseTokenRequest()
                            }else{


                            }
                            println(result)

                        }, { error ->
                            println(error)
                            RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)
                        })

        )

    }

    fun getUser(): Single<User>{

        return apiService.getUser()

    }
    fun testPush(requestBody: RequestBody):Single<ResponseBody> {

       return  apiService.sendPush(requestBody)
    }

    fun setChildren(arrChildren: List<Child>){

        if (!arrChildren.isEmpty()){

            (fragment as ChildrenListFragment).objChildrenAdapter?.refreshData(arrChildren)
        }
    }

    fun registerFirebaseToken(requestBody: RequestBody): Single<ResponseBody>{

        return apiService.registerFirebaseToken(requestBody)
    }

    fun createChild(child: Child): Single<Child>{

        return apiService.createChild(child)
    }

    fun getChildren(parentId: Int): Observable<List<Child>>{

        return apiService.getChildren(parentId)
    }
}