package com.mts.childrenbigbrother.viewmodel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.databinding.ObservableField
import android.support.v4.app.Fragment
import android.view.View
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.database.AppDatabase
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.Notification
import com.mts.childrenbigbrother.model.RetrofitErrorResponse

import com.sonin.soninandroidblankproject.networking.NetworkRequests
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import javax.inject.Inject

/**
 * Created by sakis on 6/17/2018.
 */
class NotificationListViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var activity           : Activity
    lateinit var fragment           : Fragment
    private var compositeDisposable = CompositeDisposable()
    val arrNotification             : LiveData<List<Notification>>
    val arrChildren                 : LiveData<List<Child>>
    val appDatabase                 : AppDatabase
    var obsProgrBar                 = ObservableField(View.GONE)
    @Inject
    lateinit var apiService: NetworkRequests

    init {
        ChildrenBigBrotherApp.instance?.retrofitComponent?.inject(this)
        appDatabase         = AppDatabase.getDatabase(this.getApplication<Application>())
        arrNotification     = appDatabase.notificationModel().getAllNotification()
        arrChildren         = appDatabase.childModel().getAllChildren()
    }

    fun initViewModel(activity: Activity, fragment: Fragment){

        this.activity       = activity
        this.fragment       = fragment


    }

    fun sendPushRequest(notification: Notification){

        notification.insertItem(appDatabase)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe ({
                    result ->
                    notification.notificationId = result

                    compositeDisposable.add(
                            sendPush(notification)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe ({
                                        result ->

                                    }, { error ->
                                        println(error)
                                        notification.blnFailed = true
                                        notification.updateItem(appDatabase)
                                        RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                                        error.printStackTrace()
                                    })

                    )
                })






    }


    fun sendPush(notification: Notification): Single<ResponseBody> {

       return  apiService.sendPush(notification)

    }
}