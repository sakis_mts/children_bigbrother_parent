package com.mts.childrenbigbrother.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.database.AppDatabase
import com.mts.childrenbigbrother.model.Child


/**
 * Created by sakis on 4/22/2018.
 */
class ChildrenMapViewModel(application: Application) : AndroidViewModel(application) {

    val arrChildren                 : LiveData<List<Child>>
    val appDatabase                 : AppDatabase
    init {

        appDatabase     = AppDatabase.getDatabase(this.getApplication<Application>())
        arrChildren     = appDatabase.childModel().getAllChildren()
    }
}