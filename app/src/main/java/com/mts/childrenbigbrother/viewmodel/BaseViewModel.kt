package com.mts.childrenbigbrother.viewmodel

import android.accounts.AccountManager
import android.app.Activity
import android.databinding.ObservableField
import android.support.v4.app.Fragment
import android.view.View
import com.mts.childrenbigbrother.BuildConfig
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidcommonlib.networking.CustomMoshiConverterFactory
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by sakis on 2/18/2018.
 */
class BaseViewModel {

    var obsProgrBar                 = ObservableField(View.GONE)
    private var compositeDisposable = CompositeDisposable()
    lateinit var activity           : Activity
    lateinit var fragment           : Fragment
    lateinit var apiService         : NetworkRequests

    init {

    }

    fun initViewModel(activity: Activity, fragment: Fragment){

        this.activity       = activity
        this.fragment       = fragment
        val strAuthToken    = SessionManager.instance.getAccessTokenType() + " " + SessionManager.instance.getAccessToken()
      //  this.apiService     =RetrofitServiceGenerator.getServiceUsingAuthorization(ApiService::class.java,activity,strAuthToken, SessionManager.instance.getLoggedInUsername(), AccountManager.get(activity), BuildConfig.ENDPOINT, CustomMoshiConverterFactory.create(), activity.resources.getString(R.string.account_type), ChildrenBigBrotherApp.networkTimeout  )

    }
}