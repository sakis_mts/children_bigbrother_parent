package com.mts.childrenbigbrother.viewmodel

import android.app.Activity
import android.app.Application

import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import android.databinding.ObservableField
import android.preference.PreferenceManager

import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import com.mts.childrenbigbrother.BuildConfig
import com.sonin.soninandroidcommonlib.models.AccessToken
import com.sonin.soninandroidcommonlib.networking.OAuthService
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator
import io.reactivex.Observable
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.model.RetrofitErrorResponse
import com.sonin.android_common.networking.SessionManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast
import javax.inject.Inject

/**
 * Created by sakis on 2/10/2018.
 */
class LoginViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var activity           : Activity
    lateinit var fragment           : Fragment

    private val strGrandType        = "password"
    var obsProgrBar                 = ObservableField(View.GONE)
    private var compositeDisposable = CompositeDisposable()
    var obsUsername                 = ObservableField("athan@sonin.com")
    var obsPassword                 = ObservableField("123456")

    @Inject
    lateinit var loginService: OAuthService

    init {
        ChildrenBigBrotherApp.instance?.retrofitComponent?.inject(this)
    }
    fun initViewModel(activity: Activity, fragment: Fragment){

        this.activity       = activity
        this.fragment       = fragment


    }


    fun getAccessToken(username: String, password: String) : Observable<AccessToken> {

        return loginService.getAccessToken(strGrandType, username, password, activity.getString(R.string.client_id), activity.getString(R.string.client_secret))

    }

    fun loginRequest(){

        obsProgrBar.set(View.VISIBLE)
        compositeDisposable.add(
                getAccessToken(obsUsername.get(),obsPassword.get())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())

                        .subscribe ({
                            result ->

                            obsProgrBar.set(View.GONE)
                            SessionManager.instance.setAccount(activity,result,activity.getString(R.string.account_type),obsUsername.get().toString(),true, true)
                            PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean("Logged", true).commit()
                            ChildrenBigBrotherApp.instance.updateDagger(SessionManager.instance.getAuthorizationAccessTokenType(),obsUsername.get().toString(),activity.getString(R.string.account_type))
                            println(SessionManager.instance.getAuthorizationAccessTokenType())
                        }, { error ->


                            RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                           println(error)
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)
                        })


        )


    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}