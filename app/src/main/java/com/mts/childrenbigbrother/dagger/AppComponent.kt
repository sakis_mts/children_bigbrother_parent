package com.sonin.soninandroidblankproject.dagger

import com.mts.childrenbigbrother.viewmodel.*
import dagger.Component
import javax.inject.Singleton

/**
 * Created by athanasios.moutsioul on 23/02/2018.
 */
@Singleton
@Component(modules = [AppModule::class, RetrofitModule::class])

interface AppComponent {

    fun inject(target: LoginViewModel)
    fun inject(target: NavChildrenListViewModel)
    fun inject(target: NotificationListViewModel)
    fun inject(target: RegisterViewModel)

}