package com.sonin.soninandroidblankproject.dagger

import android.accounts.AccountManager
import android.content.Context
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidcommonlib.accountManager.AccountGeneral
import com.sonin.soninandroidcommonlib.networking.CustomMoshiConverterFactory
import com.sonin.soninandroidcommonlib.networking.OAuthService
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by athanasios.moutsioul on 22/02/2018.
 */
@Module
class RetrofitModule (val strEndpoint: String, val timeout: Long, val accessToken: String?=null, val strUserName: String?=null, val accountType: String?=null, var converter : Converter.Factory?= null) {

    @Provides
    @Singleton
    @Named("WithoutAccessToken")
    fun provideOkhttpClientWithoutAccessToken(): OkHttpClient {

        val okHttpBuilder       : OkHttpClient.Builder      =  OkHttpClient.Builder()
        val interceptorLogging  : HttpLoggingInterceptor =  HttpLoggingInterceptor()

        interceptorLogging.level = HttpLoggingInterceptor.Level.BODY

        val interceptor         : Interceptor = Interceptor {

            it.proceed(it.request().newBuilder().addHeader("Accept", "application/json").build())

        }

        okHttpBuilder.addInterceptor(interceptor)
        okHttpBuilder.addInterceptor(interceptorLogging).build();

        val okHttpClient    = okHttpBuilder.readTimeout(30,TimeUnit.SECONDS).writeTimeout(30,TimeUnit.SECONDS).build()
        return okHttpClient
    }

    @Provides
    @Singleton
    @Named("WithAccessToken")
    fun provideOkhttpClientWithAccessToken(accountManager: AccountManager): OkHttpClient {

        val interceptorLogging      = HttpLoggingInterceptor()
        interceptorLogging.level    = HttpLoggingInterceptor.Level.BODY
        val okHttpBuilder           = OkHttpClient.Builder()

        val strAuthorizationKey     = SessionManager.instance.getAuthorizationAccessTokenType()

        //add headers
        val interceptor         : Interceptor = Interceptor {

            it.proceed(it.request().newBuilder()
                    .addHeader("Authorization", strAuthorizationKey)
                    .addHeader("Accept", "application/json").build())

        }



        accessToken?.let {

            okHttpBuilder.authenticator { route, response ->

                val account = AccountGeneral.getAccountFromUsername(accountManager, strUserName!!, accountType!!)

                account?.let { AccountGeneral.invalidateAuthToken(accountManager, it, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS) }

                null
            }

        }

        okHttpBuilder.addNetworkInterceptor(interceptor);
        okHttpBuilder.addInterceptor(interceptorLogging).build();
        okHttpBuilder.cache(null);
        var okHttpClient    = okHttpBuilder.readTimeout(timeout,TimeUnit.SECONDS).writeTimeout(timeout,TimeUnit.SECONDS).build()
        return okHttpClient
    }

    @Provides
    @Singleton
    fun provideAccountManager(context: Context): AccountManager {
       return AccountManager.get(context)
    }



    @Provides
    @Singleton
    @Named("RetrofitWithoutAccessToken")
    fun provideRetrofitWithoutAccessToken(@Named("WithoutAccessToken") okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(strEndpoint)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build()


    }

    @Provides
    @Singleton
    @Named("RetrofitWithAccessToken")
    fun provideRetrofitWithAccessToken(@Named("WithAccessToken") okHttpClient: OkHttpClient): Retrofit {

        var responseConverter =  CustomMoshiConverterFactory.create() as Converter.Factory

        if (converter!=null){

            responseConverter = converter as Converter.Factory

        }

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(strEndpoint)
                .addConverterFactory(responseConverter)
                .client(okHttpClient)
                .build()

    }

    @Provides
    @Singleton
    fun provideItemServiceWithoutAccessToken(@Named("RetrofitWithoutAccessToken")  retrofit: Retrofit): OAuthService {
        return retrofit.create<OAuthService>(OAuthService::class.java!!)
    }

    @Provides
    @Singleton
    fun  provideItemServiceWithAccessToken(@Named("RetrofitWithAccessToken") retrofit: Retrofit): NetworkRequests {

        return retrofit.create<NetworkRequests>(NetworkRequests::class.java!!)

    }

}