package com.mts.childrenbigbrother.model

import org.json.JSONArray
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException

/**
 * Created by sakis on 2/10/2018.
 */
data class RetrofitErrorResponse (var arrMessages : MutableList<String>){

    companion object {

        fun onError(throwable: Throwable) : String? {
            if (throwable is HttpException) {
                // We had non-2XX http error

                if (throwable.code() >=300){

                    return  "Oops something went wrong"

                }

                return fromJson(JSONObject((throwable as HttpException).response().errorBody()?.string()))

            }
            if (throwable is IOException) {
                // A network or conversion error happened

                return  "Oops something went wrong"
            }

            else return null
            // We don't know what happened. We need to simply convert to an unknown error
            // ...
        }

        fun fromJson(json : JSONObject): String?{

            var message = ""
            if (json.has("errors")){


                val intervention = json.get("errors")

                if (intervention is JSONArray) {

                    var arrJsonArrayErrors = json.getJSONArray("errors")

                    if (arrJsonArrayErrors.length()>0){

                        message = arrJsonArrayErrors[0] as String
                    }

                }else{

                    message = json.getString("errors")

                }

            }else if(json.has("error")){

                message = json.getString("error")

            }

            return message

        }
    }


}