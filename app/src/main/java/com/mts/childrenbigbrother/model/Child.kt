package com.mts.childrenbigbrother.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.mts.childrenbigbrother.database.AppDatabase
import org.jetbrains.anko.doAsync
import java.io.Serializable

/**
 * Created by sakis on 3/18/2018.
 */
@Entity(tableName = "Children")
data class Child(

        @PrimaryKey(autoGenerate = false)
        var id: Int? = null,
        @Ignore
        var arrTransmition : List<ChildLocationUpdates>? = null,
        var strName: String,
        var strPhone: String,
        var strAge: String,
        var codes: String,
        var blnMale: Int,
        var parentId: Int,
        var imagePath: String?

): Serializable {
    constructor() : this(-1,null,"","","", "",0,parentId = -1,imagePath = "")
   fun deleteItem(appDatabase : AppDatabase) {
       // deleteAsyncTask(appDatabase).execute(this)
       doAsync {
           appDatabase.childModel().deleteChild(this@Child)
       }

   }

    fun insertItem(appDatabase : AppDatabase){

        doAsync {
            appDatabase.childModel().addSingleChild(this@Child)
        }

        // insertAsyncTask(appDatabase).execute(this)
    }
}