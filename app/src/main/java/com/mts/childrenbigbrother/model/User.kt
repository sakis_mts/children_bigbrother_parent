package com.mts.childrenbigbrother.model

import java.io.Serializable

/**
 * Created by sakis on 5/6/2018.
 */
class User(val id: Int, val name: String, val email:String, val phone: String, val address: String): Serializable