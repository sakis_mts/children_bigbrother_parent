package com.mts.childrenbigbrother.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by sakis on 4/22/2018.
 */
@Entity(tableName = "ChildLocationUpdates", foreignKeys = arrayOf(ForeignKey(entity = Child::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("child_id") )))
class ChildLocationUpdates(

        @PrimaryKey(autoGenerate = false)
        var id          : Int,
        val code        : String,
        val child_id    : Int,
        val latitude    : String,
        val longitude   : String,
        val date        : Int

        ): Serializable {
}