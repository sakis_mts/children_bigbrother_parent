package com.mts.childrenbigbrother.model

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.mts.childrenbigbrother.database.AppDatabase
import io.reactivex.Single
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.Serializable

/**
 * Created by sakis on 5/27/2018.
 */
@Entity(tableName = "Notification")
data class Notification(
        @PrimaryKey(autoGenerate = true)
        var notificationId: Long ?= null,
        val notificationType: Int,
        val strMessage: String,
        var blnFailed : Boolean ?= null,
        val strChildName : String,
        val strMisc: String,
        val dateCreated: Long?
                ) : Serializable {

        companion object {

                val INCOMING = 1
                val OUTGOING = 2
        }
        //    constructor(id: Int? = null, strName: String,  strAge: String,  blnMale: Boolean) : this(strName, strAge, blnMale)
        fun deleteItem(appDatabase : AppDatabase) {
                // deleteAsyncTask(appDatabase).execute(this)
                doAsync {
                        appDatabase.notificationModel().deleteNotification(this@Notification)
                }

        }

        fun insertItem(appDatabase : AppDatabase): Single<Long>{

                return Single.fromCallable({ appDatabase.notificationModel().addSingleNotification(this@Notification)})


                // insertAsyncTask(appDatabase).execute(this)
        }

        fun updateItem(appDatabase : AppDatabase){

                doAsync {
                        appDatabase.notificationModel().updateNotification(this@Notification)
                }

                // insertAsyncTask(appDatabase).execute(this)
        }
}
