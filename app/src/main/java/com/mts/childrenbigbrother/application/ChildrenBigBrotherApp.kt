package com.mts.childrenbigbrother.application

import android.app.Application
import android.os.Environment
import com.mts.childrenbigbrother.BuildConfig
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.networking.CustomMoshiConverterFactory
import com.mts.childrenbigbrother.view.LoginActivity
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.dagger.AppComponent
import com.sonin.soninandroidblankproject.dagger.AppModule
import com.sonin.soninandroidblankproject.dagger.DaggerAppComponent
import com.sonin.soninandroidblankproject.dagger.RetrofitModule
import android.os.Environment.getExternalStorageDirectory
import android.preference.PreferenceManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.mts.childrenbigbrother.model.User
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


/**
 * Created by sakis on 2/11/2018.
 */
class ChildrenBigBrotherApp : Application() {



    companion object {
        lateinit var instance: ChildrenBigBrotherApp
            private set
        //var user                    : User? = null
        var blntokenRefresh         = false
        val BDK_FIREBASE_ID         = "FirebaseToken"
        val PREFS_NAME              = "Device Prefs"
        var intUnreadNotifications  = 0
        val networkTimeout          :Long= 30
        val BDK_USER                = "bdk_user"
        val user : User by lazy {
            var prefs = PreferenceManager.getDefaultSharedPreferences(instance)
            val strUser = prefs.getString(BDK_USER,"")
            Gson().fromJson(strUser, User::class.java)

        }
    }

    var retrofitComponent: AppComponent? = null
    private fun initDagger(app: ChildrenBigBrotherApp,module: RetrofitModule?): AppComponent {
        if (module!=null){
            return DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .retrofitModule(module)
                    .build()

        }else{
            return  DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .retrofitModule(RetrofitModule(BuildConfig.ENDPOINT, 30))
                    .build()

        }

    }

    override fun onCreate() {
        super.onCreate()
        instance        = this

        FirebaseInstanceId.getInstance()?.let {

            println("Firebase Token: ${it.token}")
        }
        try {
            val sd = Environment.getExternalStorageDirectory()

            if (sd.canWrite()) {
                val currentDBPath = "/data/data/$packageName/databases/children_bb_db"
                val backupDBPath = "backupname.db"
                val currentDB = File(currentDBPath)
                val backupDB = File(sd, backupDBPath)

                if (currentDB.exists()) {
                    val src = FileInputStream(currentDB).getChannel()
                    val dst = FileOutputStream(backupDB).getChannel()
                    dst.transferFrom(src, 0, src.size())
                    src.close()
                    dst.close()
                }
            }
        } catch (e: Exception) {

        }



        SessionManager.init(this, NavigationActivity::class.java, LoginActivity::class.java, null)

        //init Dagger Retrofit Module
        if (SessionManager.instance.isLoggedIn()){

            val module              = RetrofitModule(BuildConfig.ENDPOINT, 30,SessionManager.instance.getAccessToken(),SessionManager.instance.getLoggedInUsername(),SessionManager.instance.getAccessTokenType(),CustomMoshiConverterFactory.createWithNull())
            retrofitComponent       = initDagger(this,module)

        }else{

            retrofitComponent       = initDagger(this,null)

        }

    }

    fun updateDagger(accessToken: String, strUsername:String,accountType:String ){

        retrofitComponent   = null
        val module          = RetrofitModule(BuildConfig.ENDPOINT, 30,accessToken,strUsername,accountType, CustomMoshiConverterFactory.createWithNull())

        retrofitComponent = initDagger(this, module)


    }
}