package com.mts.childrenbigbrother.view

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.annotation.Nullable
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.model.Child
import com.sonin.soninandroidcommonlib.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_generic_add_child.*
import android.widget.RadioGroup
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.utils.AgeCalculation
import com.mts.childrenbigbrother.utils.ImageResizer
import com.sonin.soninandroidcommonlib.utils.PermissionManager
import org.jetbrains.anko.support.v4.toast
import java.io.*
import java.time.LocalDate
import java.time.Period
import java.util.*


/**
 * Created by sakis on 3/18/2018.
 */
class DialogFragmentAddChild : DialogFragment() , Serializable, PermissionManager.PermissionListener{

    private val REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION    = 1
    private val REQUEST_IMAGE_CAPTURE                       = 118
    private val REQUEST_CODE_PHOTO                          = 117

    interface OnSaveListener {
        fun onChildSave(child: Child)
    }
    companion object {

        val BDK_BTN_SAVE_CALLBACK   = "yes_callback"
        val IMAGE_DIRECTORY         = "/ChildrenBigBrother"

        fun newInstance(listener : OnSaveListener ): DialogFragment {

            val fragment = DialogFragmentAddChild()
            var bundle = Bundle()
            fragment.setTargetFragment(listener as Fragment, 117);
            fragment.arguments = bundle
            return fragment

        }

    }

    var callbackSave            : OnSaveListener? = null
    var objNewChild             : Child? = null
    lateinit var etAge          : EditText
    lateinit var etName         : EditText
    lateinit var rgGender       : RadioGroup
    lateinit var radio_male     : RadioButton
    lateinit var radio_female   : RadioButton
    var strImagePath            = ""

    private val GALLERY = 1
    private val CAMERA  = 2

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFragmetStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view = inflater.inflate(R.layout.dialog_generic_add_child, container)

        initVars(view)
        return view
    }

    fun initVars(view: View) {

        callbackSave        = targetFragment as OnSaveListener

        etAge               = view.findViewById<EditText>(R.id.etAge)
        etName              = view.findViewById<EditText>(R.id.etName)
        val ivChild         = view.findViewById<ImageView>(R.id.ivChild)
        val btnSave         = view.findViewById<Button>(R.id.btnSave)
        val btnCancel       = view.findViewById<Button>(R.id.btnCancel)
        rgGender            = view.findViewById<RadioGroup>(R.id.rgGender)
        radio_male          = view.findViewById<RadioButton>(R.id.radio_male)
        radio_female        = view.findViewById<RadioButton>(R.id.radio_female)

        Picasso.with(view.context).load(R.drawable.ic_profile_placeholder).fit().transform(CircleTransform()).into(ivChild)

       etAge.setOnClickListener { displayDatePicker() }

        btnCancel.setOnClickListener { dismiss() }
        btnSave.setOnClickListener {

            if (!etName.text.isEmpty() && !etAge.text.isEmpty()){

                generateChild()

                objNewChild?.let {child->

                    callbackSave?.let { it1 ->

                        it1.onChildSave(child)

                    }
                }

                dismiss()
            }else{

                toast("Age and Name should be completed")
            }



        }

            ivChild.setOnClickListener {

                PermissionManager.requestPermissionsIfNecessary(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), this, REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION) }
        }

    fun displayDatePicker(){

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in textbox
           etAge.setText("$dayOfMonth/${monthOfYear+1}/$year")

            val calendar = Calendar.getInstance()
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            calendar.set(Calendar.YEAR,year)
            println(calendar.timeInMillis/1000L)

           val tmp = AgeCalculation().getResult(year, monthOfYear+1, dayOfMonth)


        }, year, month, day)
        dpd.show()

    }
    fun generateChild(){

        var blnMale              = 0
        val checkedRadioButtonId = rgGender.getCheckedRadioButtonId()

        if (checkedRadioButtonId!= -1 && checkedRadioButtonId == R.id.radio_male) {

            blnMale = 1

        }
        if (!etName.text.isEmpty() && !etAge.text.isEmpty()){


            objNewChild = Child(parentId = ChildrenBigBrotherApp.user.id, strName =etName.text.toString(),codes = "",strAge =  etAge.text.toString() ,blnMale = blnMale, strPhone = etPhoneNumber.text.toString(),imagePath = strImagePath)

        }

    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(activity)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(pictureDialogItems
        ) { dialog, which ->

            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

       startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.

        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {

            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(activity,
                    arrayOf(f.getPath()),
                    arrayOf("image/jpeg"), null)
            fo.close()

            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        /* if (resultCode == this.RESULT_CANCELED)
         {
         return
         }*/
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                val contentURI = data!!.data
                try
                {
                    val bitmap = MediaStore.Images.Media.getBitmap(activity.contentResolver, contentURI)
                    strImagePath = saveImage(bitmap)
                    Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                    ivChild!!.setImageBitmap(bitmap)
                    Picasso.with(context).load(File(strImagePath)).transform(com.mts.childrenbigbrother.utils.CircleTransform()).into(ivChild)


                    var resizedBitmap: Bitmap? = null
                    val file: File
                    file = File(strImagePath)
                    try {

                        resizedBitmap = ImageResizer.rotateImageIfRequired(bitmap, activity, contentURI);
                        val out = FileOutputStream(file);
                        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                        out.flush();
                        out.close();

                        Picasso.with(context).load(File(file.toURI())).transform(com.mts.childrenbigbrother.utils.CircleTransform()).into(ivChild)


                    } catch (e:FileNotFoundException) {
                        e.printStackTrace();
                    }
                }
                catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            strImagePath = saveImage(thumbnail)
            Picasso.with(context).load(File(strImagePath)).transform(com.mts.childrenbigbrother.utils.CircleTransform()).into(ivChild)
            Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()

    }


    override fun onPermissionAlreadyGranted() {
        showPictureDialog()
    }

    override fun onExplainPermission() {
        // Toast.makeText(getActivity(), "This permission is required to choose your profile image", Toast.LENGTH_LONG).show();
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                showPictureDialog()

            } else {

                onExplainPermission()
            }
        }
    }
}
