package com.mts.childrenbigbrother.view

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.adapter.NotificationAdapter
import com.mts.childrenbigbrother.databinding.FragmentNotificationListBinding
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.Notification
import com.mts.childrenbigbrother.viewmodel.NotificationListViewModel
import kotlinx.android.synthetic.main.fragment_notification_list.*
import kotlinx.android.synthetic.main.fragment_notification_list.view.*
import org.jetbrains.anko.doAsync


/**
 * Created by sakis on 6/17/2018.
 */
class NotificationListFragment : Fragment() {

    lateinit var fragmentNotificationListBinding    : FragmentNotificationListBinding
    lateinit var notificationListViewModel          : NotificationListViewModel
    lateinit var objNotificationAdapter             : NotificationAdapter

    var arrChildren = mutableListOf<Child>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }

        initViewModel()

        fragmentNotificationListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_list, container, false)
        fragmentNotificationListBinding.notificationListViewModel = notificationListViewModel

        setHasOptionsMenu(true)
        return fragmentNotificationListBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setLiveDataObservers()
        initVars()


    }


    fun initViewModel() {

        notificationListViewModel = ViewModelProviders.of(this).get(NotificationListViewModel::class.java)
        notificationListViewModel.initViewModel(activity, this)

    }

    fun initVars() {

        objNotificationAdapter = NotificationAdapter(activity, mutableListOf<Notification>(), arrChildren, this::objClicked)
        fragmentNotificationListBinding.root.rvNotification.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = objNotificationAdapter
        }

        btnAddNotification.setOnClickListener {


            DialogFragmentCreateNotification.newInstance(this::sendNotification,ArrayList(arrChildren) ).show(activity.supportFragmentManager, "FragmentDialog")
        }

    }

    fun sendNotification(notification: Notification){

        notificationListViewModel.sendPushRequest(notification)
    }

    fun setLiveDataObservers() {

        notificationListViewModel.arrNotification.observe(this, Observer {

            it?.let { notifications ->

                objNotificationAdapter.refreshData(notifications)
            }


        })

        notificationListViewModel.arrChildren.observe(this, Observer {

            it?.let {

                if (!it.isEmpty()!!){
                    arrChildren.clear()
                    arrChildren = it.toMutableList()
                    objNotificationAdapter.refreshChildrenData(it.toMutableList())


                }
            }


        })



    }

    fun objClicked(notification: Notification, index:Int){


    }





}