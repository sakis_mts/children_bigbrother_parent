package com.mts.childrenbigbrother.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.databinding.FragmentRegisterBinding
import com.mts.childrenbigbrother.viewmodel.RegisterViewModel
import com.sonin.soninandroidcommonlib.validation.DefaultNotEmptyPolicy
import com.sonin.soninandroidcommonlib.validation.Field
import com.sonin.soninandroidcommonlib.validation.Form
import kotlinx.android.synthetic.main.fragment_register.*
import org.jetbrains.anko.support.v4.toast

class RegisterFragment : Fragment() {

    private  lateinit var registerViewModel        : RegisterViewModel
    lateinit var fragmentRegisterBinding           : FragmentRegisterBinding
    private val form                               : Form by lazy { Form() }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }

        initViewModel()


        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        fragmentRegisterBinding.registerViewModel = registerViewModel

        setHasOptionsMenu(true)
        return fragmentRegisterBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initVars()
        initForm()

    }


    fun initViewModel(){

        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)

    }


    fun initVars(){

      btnRegister.setOnClickListener {


          if (form.validateForm()){

              if ((etPassword.text.toString().equals(etRePassword.text.toString()))){
                  clearInputErrorTips()
                  registerViewModel.registerParentRequest()
              }else{

                  toast("Passwords don't match")

              }

          }


      }

        registerViewModel.command.observe(this, Observer {

            it?.let {

                if (!it.blnSuccess){

                    toast(it.strMessage)

                }
            }


        })
        tvCancel.setOnClickListener {

            activity.supportFragmentManager.popBackStack()
        }


    }

    private fun initForm() {

        form.addField(Field.FieldBuilder(etFirstName, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etLastName, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etPassword, Field.FieldType.PASSWORD).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etRePassword, Field.FieldType.PASSWORD).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etEmail, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etAddress, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etPhoneNumber, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.setCustomFailure { view, _ ->

            when (view.id){

                R.id.etFirstName -> {
                    etFirstName.error = resources.getString(R.string.register_fields_error)

                }
                R.id.etLastName -> {
                    etLastName.error = resources.getString(R.string.register_fields_error)

                }
                R.id.etPassword -> {
                    etPassword.error = resources.getString(R.string.register_fields_error)

                }
                R.id.etRePassword -> {
                    etRePassword.error = resources.getString(R.string.register_fields_error)

                }
                R.id.etEmail -> {
                    etEmail.error = resources.getString(R.string.register_fields_error)

                }
                R.id.etAddress -> {
                    etAddress.error = resources.getString(R.string.register_fields_error)

                }
                R.id.etPhoneNumber -> {
                    etPhoneNumber.error = resources.getString(R.string.register_fields_error)

                }

            }

        }
    }

    fun clearInputErrorTips(){

        etPhoneNumber.error = null
        etAddress.error     = null
        etEmail.error       = null
        etFirstName.error   = null
        etLastName.error    = null
        etPassword.error    = null
        etRePassword.error  = null

    }
}