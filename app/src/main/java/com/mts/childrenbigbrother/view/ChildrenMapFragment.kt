package com.mts.childrenbigbrother.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.viewmodel.ChildrenMapViewModel
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mts.childrenbigbrother.model.Child
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.doAsync
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.CameraUpdate






/**
 * Created by sakis on 4/22/2018.
 */
class ChildrenMapFragment : Fragment(), OnMapReadyCallback {


    private  lateinit var childrenMapViewModel          : ChildrenMapViewModel
    var  mMap                                           :  GoogleMap? = null
    var arrChildLatestLocation                          = HashMap<Child,Location>()
    var builder                                         = LatLngBounds.Builder()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }

        initViewModel()


        val view = inflater?.inflate( R.layout.fragment_children_map, container, false)

        setHasOptionsMenu(true)
        initVars(view)
        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        setLiveDataObservers()
    }

    fun setLiveDataObservers(){

        childrenMapViewModel.arrChildren.observe(this, Observer {
            arrChildLatestLocation.clear()
            it?.let {children ->

                doAsync  {
                children.forEach {

                    var arrLocationUpdates = childrenMapViewModel.appDatabase.transmitionLocationModel().getAllLocationUpdatesByChild(it.id!!)
                    it.arrTransmition = arrLocationUpdates


                    }
                    arrChildLatestLocation.clear()
                    children.map {child->

                        val objLatestUpdate = child.arrTransmition?.maxBy { it.date }
                        objLatestUpdate?.let {

                            val objLocation     = Location("")
                            val lat             = it.latitude.toDoubleOrNull()
                            val lng             = it.longitude.toDoubleOrNull()
                            builder.include(LatLng(lat!!,lng!!))

                            if (lat!= null && lng!== null){

                                objLocation.latitude    = lat
                                objLocation.longitude   = lng
                                arrChildLatestLocation.put(child, objLocation)

                            }


                        }

                    }
                    mMap?.let {

                        updateMarkers(arrChildLatestLocation)

                    }
                }

        }

    })

    }



    fun updateMarkers(arrChildLatestLocation: HashMap<Child,Location> ){

        async(UI){
            arrChildLatestLocation.forEach {

                mMap?.addMarker( MarkerOptions().position(LatLng(it.value.latitude, it.value.longitude))
                        .title(it.key.strName));
            }

            val bounds = builder.build()
            val width = resources.displayMetrics.widthPixels
            val height = resources.displayMetrics.heightPixels
            val padding = (width * 0.10).toInt() // offset from edges of the map 10% of screen

            val cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)

            mMap?.animateCamera(cu)

        }



    }
    override fun onMapReady(map: GoogleMap) {
        mMap = map
       // updateMarkers(arrChildLatestLocation)

    }
    fun initViewModel(){

        childrenMapViewModel = ViewModelProviders.of(this).get(ChildrenMapViewModel::class.java)


    }


    fun initVars(view: View?){

        val mapFragment = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)

        mapFragment.getMapAsync(this)

    }
}