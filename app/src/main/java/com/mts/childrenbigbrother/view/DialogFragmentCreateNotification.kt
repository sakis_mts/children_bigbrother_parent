package com.mts.childrenbigbrother.view

import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.Notification
import com.sonin.soninandroidcommonlib.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_generic_add_child.*
import org.jetbrains.anko.support.v4.toast
import org.json.JSONObject
import java.io.Serializable
import java.util.*
import android.content.pm.PackageManager
import android.support.annotation.NonNull
import com.sonin.soninandroidcommonlib.utils.PermissionManager


/**
 * Created by sakis on 6/17/2018.
 */
class DialogFragmentCreateNotification  : DialogFragment() {

    companion object {

        val BDK_BTN_SAVE_CALLBACK   = "yes_callback"
        val BDK_CHILDREN            = "children"


        fun newInstance(onSendClicked: ((notification: Notification) -> Unit)?, arrChild: ArrayList<Child>): DialogFragment {

            val fragment = DialogFragmentCreateNotification()
            var bundle = Bundle()
            bundle.putSerializable(BDK_CHILDREN, arrChild)
            bundle.putSerializable(BDK_BTN_SAVE_CALLBACK, onSendClicked as Serializable?)
            fragment.arguments = bundle
            return fragment

        }
    }

    var callbackSend            : ((notification: Notification) -> Unit)? = null
    var objNewNotification      :Notification? = null
    var arrChildren             = listOf<Child>()

    lateinit var spChildren             : Spinner
    lateinit var spNotification         : Spinner
    lateinit var etMessage              : EditText
    var strMesageTosend                 =""


    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFragmetStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view = inflater.inflate(R.layout.fragment_dialog_create_notification, container)

        initVars(view)
        return view
    }

    fun initVars(view: View) {

        if (arguments != null && arguments.containsKey(BDK_BTN_SAVE_CALLBACK)) {

            callbackSend = arguments.getSerializable(BDK_BTN_SAVE_CALLBACK) as ((notification: Notification) -> Unit)?
        }
        if (arguments != null && arguments.containsKey(BDK_CHILDREN)) {

            arrChildren = arguments.getSerializable(BDK_CHILDREN) as List<Child>
        }
        spChildren              = view.findViewById<Spinner>(R.id.spChildren)
        spNotification          = view.findViewById<Spinner>(R.id.spNotification)
        etMessage               = view.findViewById<EditText>(R.id.etMessage)
        val btnSend             = view.findViewById<Button>(R.id.btnSend)
        val btnCancel           = view.findViewById<Button>(R.id.btnCancel)



        val arrChildrenName = arrChildren.map { it.strName }.toTypedArray()
        val sp_adapterChildren  = com.mts.childrenbigbrother.adapter.SpinnerAdapter(activity,  R.layout.spinner_item,arrChildrenName)
        sp_adapterChildren.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spChildren.adapter = sp_adapterChildren

        val sp_adapter  = com.mts.childrenbigbrother.adapter.SpinnerAdapter(activity,  R.layout.spinner_item,getResources().getStringArray( R.array.notification_array))
        sp_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spNotification.adapter = sp_adapter

        spNotification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == getResources().getStringArray( R.array.notification_array).size-1 ){

                   etMessage.visibility = View.VISIBLE

                }else{
                    strMesageTosend =  getResources().getStringArray( R.array.notification_array).get(position)
                    etMessage.visibility = View.GONE
                }
            }


        }

        btnCancel.setOnClickListener { dismiss() }
        btnSend.setOnClickListener {

            if (arrChildren.isEmpty()){

                toast("You don't have any children. Please first register your child")

            }else{

                if (spNotification.selectedItemPosition == getResources().getStringArray( R.array.notification_array).size-1){

                    strMesageTosend = etMessage.text.toString()
                }
                callbackSend?.let {

                    val misc = JSONObject()
                    val child = arrChildren.firstOrNull { it.strName.equals(spChildren.selectedItem as String) }
                    child?.let {
                        misc.put("userId",child.id )
                        misc.put("type","child" )
                        misc.put("senderName", ChildrenBigBrotherApp.user.name)
                    }
                    val strChildName = child?.let {it.strName}


                    it(Notification(notificationType = Notification.OUTGOING,strMessage = strMesageTosend,strMisc = misc.toString(),strChildName =  strChildName ?: "", dateCreated = System.currentTimeMillis()/1000))

                }
            }

            dismiss()

        }
    }

    override fun onPause() {
        super.onPause()
        dismiss()
    }



}
