package com.mts.childrenbigbrother.view

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mts.childrenbigbrother.R

import com.mts.childrenbigbrother.databinding.FragmentLoginBinding
import com.mts.childrenbigbrother.viewmodel.LoginViewModel
import com.sonin.soninandroidcommonlib.validation.DefaultNotEmptyPolicy
import com.sonin.soninandroidcommonlib.validation.Field
import com.sonin.soninandroidcommonlib.validation.Form
import kotlinx.android.synthetic.main.fragment_login.*
import org.jetbrains.anko.support.v4.act


/**
 * Created by sakis on 2/10/2018.
 */
class LoginFragment : Fragment() , View.OnClickListener {


    private  lateinit var loginViewModel        : LoginViewModel
    lateinit var loginFragmentBinding           : FragmentLoginBinding
    private val form                            : Form by lazy { Form() }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }

        initViewModel()


        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        loginFragmentBinding.loginViewModel = loginViewModel

        setHasOptionsMenu(true)
        return loginFragmentBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initVars()
        initForm()

    }
    private fun initForm() {

        form.addField(Field.FieldBuilder(etUsername, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etPassword, Field.FieldType.PASSWORD).policy(DefaultNotEmptyPolicy()).createField())
        form.setCustomFailure { view, _ ->

            when (view.id){

                R.id.etUsername -> {
                    etUsername.error = resources.getString(R.string.register_email_validation)

                }
                R.id.etPassword -> {
                    etPassword.error = resources.getString(R.string.register_password_validation)

                }

            }

        }
    }

    fun initViewModel(){

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginViewModel.initViewModel(activity,this)

    }


    fun initVars(){

        loginFragmentBinding.btnLogin.setOnClickListener(this)
        loginFragmentBinding.btnRegister.setOnClickListener(this)


    }

    override fun onClick(v: View) {

        when(v.id){
            R.id.btnLogin ->{
                if (form.validateForm()){

                    clearInputErrorTips()
                    loginViewModel.loginRequest()}
                }

            R.id.btnRegister->{

                activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, RegisterFragment())
                        .addToBackStack(null)
                        .commit()

            }


        }

    }

    fun clearInputErrorTips(){

        etUsername.error = null
        etPassword.error = null

    }
}