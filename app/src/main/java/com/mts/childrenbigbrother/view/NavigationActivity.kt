package com.mts.childrenbigbrother

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.firebase.iid.FirebaseInstanceId
import com.mts.childrenbigbrother.database.AppDatabase
import com.mts.childrenbigbrother.model.User
import com.mts.childrenbigbrother.view.ChildrenListFragment
import com.mts.childrenbigbrother.view.ChildrenMapFragment
import com.mts.childrenbigbrother.view.NotificationListFragment
import com.sonin.android_common.networking.SessionManager
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.app_bar_navigation.*
import kotlinx.android.synthetic.main.content_navigation.*

class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,TabLayout.OnTabSelectedListener  {

    lateinit var adapter                : PagerAdapter
    lateinit var toggle                 : ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        //For future development
        initDrawer()
        initToolbar()
        initTablayout()


    }

    fun initDrawer(){

        toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)



    }
    fun initToolbar(){

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setHomeButtonEnabled(true);
        supportActionBar?.title = "Children"


        // create our manager instance after the content view is set

    }

    fun initVars(){

    }



    private fun initTablayout() {

        tabs.addTab(tabs.newTab())
        tabs.addTab(tabs.newTab())
        tabs.addTab(tabs.newTab())
        tabs.getTabAt(0)?.setIcon(R.drawable.ic_action_children_icon)
        tabs.getTabAt(1)?.setIcon(R.drawable.ic_action_map_icon2)
        tabs.getTabAt(2)?.setIcon(R.drawable.ic_action_notification)
        tabs.getTabAt(0)?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.accent), PorterDuff.Mode.SRC_IN)
//        tabs.getTabAt(1)?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.icons), PorterDuff.Mode.SRC_IN)
        tabs.setTabGravity(TabLayout.GRAVITY_FILL)

        //changeTabsFont()

        adapter = PagerAdapter(supportFragmentManager, tabs.getTabCount())
        viewPager.setAdapter(adapter)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        viewPager.setOffscreenPageLimit(3);
        tabs.addOnTabSelectedListener(this)

    }

    private fun changeTabsFont() {

//        val vg = tabs.getChildAt(0) as ViewGroup
//        val tabsCount = vg.childCount
//
//        for (j in 0 until tabsCount) {
//
//            val vgTab = vg.getChildAt(j) as ViewGroup
//            val tabChildsCount = vgTab.childCount
//
//            for (i in 0 until tabChildsCount) {
//
//                val tabViewChild = vgTab.getChildAt(i)
//                if (tabViewChild is TextView) {
//                    tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.edittext_text_size_low))
//                    tabViewChild.typeface = ResourcesCompat.getFont(this, R.font.avenir_heavy);
//                }
//
//            }
//        }
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//
//        menuInflater.inflate(R.menu.navigation, menu)
//        //Disabled the menu
//        return true
//    }
//
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_logout -> {
               SessionManager.instance.logout(this)
                AppDatabase.destroyInstance()
            }
            R.id.nav_setting -> {

            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }



    inner class PagerAdapter(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {

            when (position) {
                0 -> {  return ChildrenListFragment() }
                1 -> {  return ChildrenMapFragment() }
                2 -> {  return NotificationListFragment() }
                else -> return null
            }
        }

        override fun getCount(): Int {
            return mNumOfTabs
        }
    }


    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        tab?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.icons), PorterDuff.Mode.SRC_IN)
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
        when(tab.position){
            0-> { supportActionBar?.title = "Children"}
            1-> { supportActionBar?.title = "Children Location"}
            2-> { supportActionBar?.title = "Notification"}
        }
        viewPager.setCurrentItem(tab?.getPosition()!!);
        tab?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.accent), PorterDuff.Mode.SRC_IN)
    }

    fun  updateNavHeaderInfo(user: User){

        val navigationView  = findViewById<View>(R.id.nav_view) as NavigationView
        val headerView      = navigationView.getHeaderView(0)
        val navUsername     = headerView.findViewById(R.id.tvHeaderName) as TextView
        val navEmail        = headerView.findViewById(R.id.tvHeaderEmail) as TextView
        navUsername.text    = user.name
        navEmail.text       = user.email

    }
}
