package com.mts.childrenbigbrother.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.model.ChildLocationUpdates
import com.mts.childrenbigbrother.utils.getDateTime
import kotlinx.android.synthetic.main.activity_child_map.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.doAsync

class ChildMapActivity : AppCompatActivity() , OnMapReadyCallback {

    companion object {

        val BDK_CHILD = "child"
    }

    var arrTransmition : List<ChildLocationUpdates>? = null
    var  mMap          :  GoogleMap? = null
    var builder        = LatLngBounds.Builder()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_child_map)
        initVars()
        initToolbar()
    }

    override fun onMapReady(map: GoogleMap) {
        mMap = map
        // updateMarkers(arrChildLatestLocation)

        if (intent.hasExtra(BDK_CHILD)){

            val child = (intent.getSerializableExtra(BDK_CHILD) as Child)
            arrTransmition = child.arrTransmition
            updateMarkers(arrTransmition as ArrayList<ChildLocationUpdates>)
            supportActionBar?.title = "${child.strName} Latest Location Updates"
        }

    }

    fun initToolbar(){

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setHomeButtonEnabled(true);
        supportActionBar?.title = "Child Latest Location Updates"

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun initVars(){

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)


    }

    override fun onBackPressed() {
        super.onBackPressed()
       finish()
    }

    fun updateMarkers(arrLocations: ArrayList<ChildLocationUpdates> ){


        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        val padding = (width * 0.10).toInt() // offset from edges of the map 10% of screen

        async(UI){
            arrLocations.forEach {

               val marker =  mMap?.addMarker( MarkerOptions()
                        .position(LatLng(it.latitude.toDouble(), it.longitude.toDouble()))
                        .title(it.date.toLong().getDateTime()))
                marker!!.showInfoWindow()
                builder.include(LatLng(it.latitude.toDouble(), it.longitude.toDouble()))
            }
            val bounds = builder.build()

            val cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)

            mMap?.animateCamera(cu)

        }



    }
}
