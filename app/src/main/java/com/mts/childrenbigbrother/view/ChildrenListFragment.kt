package com.mts.childrenbigbrother.view

import android.Manifest
import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.iid.FirebaseInstanceId
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.adapter.ChildListAdapter
import com.mts.childrenbigbrother.databinding.FragmentNavChildrenListBinding
import com.mts.childrenbigbrother.model.Child
import com.mts.childrenbigbrother.services.FirebaseIDService
import com.mts.childrenbigbrother.viewmodel.NavChildrenListViewModel
import com.sonin.soninandroidcommonlib.utils.PermissionManager
import kotlinx.android.synthetic.main.fragment_nav_children_list.*
import kotlinx.android.synthetic.main.fragment_nav_children_list.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.support.v4.defaultSharedPreferences
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.toast

/**
 * Created by sakis on 2/18/2018.
 */
class ChildrenListFragment : Fragment(), DialogFragmentAddChild.OnSaveListener,  PermissionManager.PermissionListener {



    lateinit var fragmentNavChildrenListBinding : FragmentNavChildrenListBinding
    lateinit var  navChildrenListViewModel      : NavChildrenListViewModel
    lateinit var  objChildrenAdapter            : ChildListAdapter

    private val REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION    = 1

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }

        initViewModel()

        fragmentNavChildrenListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_nav_children_list, container, false)
        fragmentNavChildrenListBinding.navChildrenListViewModel = navChildrenListViewModel

        setHasOptionsMenu(true)
        return fragmentNavChildrenListBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setLiveDataObservers()
        initVars()

    }


    fun initViewModel(){

        navChildrenListViewModel = ViewModelProviders.of(this).get(NavChildrenListViewModel::class.java)
        navChildrenListViewModel.initViewModel(activity,this)

    }

    fun initVars(){

        PermissionManager.requestPermissionsIfNecessary(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), this, REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION)
        var prefs  = PreferenceManager.getDefaultSharedPreferences(activity)
        val userId = prefs.getInt("userId",-1)
        if (userId!= -1){

            if (defaultSharedPreferences.getBoolean(FirebaseIDService.BDK_REFRESHED_TOKEN, false)){
                navChildrenListViewModel.registerFirebaseTokenRequest()
            }


        }

        navChildrenListViewModel.getUserRequest()

        objChildrenAdapter = ChildListAdapter(activity, mutableListOf<Child>(), this::objClicked)
        fragmentNavChildrenListBinding.root.rvChildren.apply {
            setHasFixedSize(true)
            layoutManager   = LinearLayoutManager(activity)
            adapter         = objChildrenAdapter
        }

        btnAddChild.setOnClickListener {

            DialogFragmentAddChild.newInstance(this).show(activity.supportFragmentManager,"FragmentDialog")
        }




    }

    fun setLiveDataObservers(){

        navChildrenListViewModel.arrChildren.observe(this, Observer {

            it?.let {children ->

                children.forEach {

                    doAsync {

                        var arrLocationUpdates  = navChildrenListViewModel.appDatabase.transmitionLocationModel().getAllLocationUpdatesByChild(it.id!!)
                        it.arrTransmition       = arrLocationUpdates
                    }

                }
                objChildrenAdapter.refreshData(children) }


        })
    }


    fun createChildRequest(child: Child){

        navChildrenListViewModel.createChildRequest(child)

    }

    fun addChild(child: Child){

        objChildrenAdapter.addChild(child)

    }

    fun objClicked(child: Child, index:Int){

        child.arrTransmition?.let { locations->

            if (locations.isEmpty()){

                toast("No location updates available for ${child.strName}")

            }else{
                activity.startActivity(activity.intentFor<ChildMapActivity>(ChildMapActivity.BDK_CHILD to child))

            }
        }


    }

    override fun onChildSave(child: Child) {

        createChildRequest(child)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

               // navChildrenListViewModel.getChildrenRequest()

            } else {

                onExplainPermission()
            }
        }
    }

    override fun onPermissionAlreadyGranted() {

       // navChildrenListViewModel.getChildrenRequest()

    }

    override fun onExplainPermission() {

    }

}