package com.mts.childrenbigbrother.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.mts.childrenbigbrother.NavigationActivity
import com.mts.childrenbigbrother.R
import org.jetbrains.anko.intentFor

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val blnLoggedIn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("Logged", false)
        if (blnLoggedIn){
           startActivity(intentFor<NavigationActivity>())

        }else{
            with(supportFragmentManager){
                beginTransaction().
                        replace(R.id.frameLayout, LoginFragment())
                        .commit()

            }

        }

    }
}
