package com.mts.childrenbigbrother.services
import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.mts.childrenbigbrother.BuildConfig
import com.mts.childrenbigbrother.view.LoginActivity

import com.sonin.soninandroidcommonlib.accountManager.AccountAuthenticator


internal class AuthenticatorService : Service() {
    override fun onBind(intent: Intent): IBinder {

        val authenticator = AccountAuthenticator(this, LoginActivity::class.java, BuildConfig.ENDPOINT)

        return authenticator.getIBinder()
    }
}