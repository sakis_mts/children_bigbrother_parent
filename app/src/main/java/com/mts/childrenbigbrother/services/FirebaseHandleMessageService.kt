package com.mts.childrenbigbrother.services

import android.app.*
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.os.IBinder
import android.support.v7.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.mts.childrenbigbrother.R
import com.mts.childrenbigbrother.database.AppDatabase
import com.mts.childrenbigbrother.model.Notification
import com.mts.childrenbigbrother.model.RetrofitErrorResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast


class FirebaseHandleMessageService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null ) {

            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            val strBody = remoteMessage.notification.body ?: ""
            val strTitle = remoteMessage.notification.title ?: ""
            sendNotification(strBody,   strTitle)

            var strChildName = ""
            if (remoteMessage.data.containsKey("senderName")){

                strChildName = remoteMessage.data.get("senderName") as String
            }

            val notification = Notification(notificationType = Notification.INCOMING,strMessage = strBody,strMisc = "",strChildName =  strChildName, dateCreated = System.currentTimeMillis()/1000)
            notification.insertItem(AppDatabase.getDatabase(this))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->
                            notification.notificationId = result

                        })


        }
    }

    private fun sendNotification(messageBody: String, title: String) {
        val intent = Intent(this, NativeActivity::class.java)
        val CHANNEL_ID = "my_channel_01"
        val name = title
        val importance = NotificationManager.IMPORTANCE_HIGH

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)


        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ID)


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
            notificationBuilder.setColor(getResources().getColor(R.color.accent))
        } else {
            notificationBuilder.setSmallIcon(R.drawable.ic_launcher)
        }
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val mChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(CHANNEL_ID, name, importance)
        } else {
            null
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    fun isForeground(myPackage: String): String {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningTaskInfo = manager!!.getRunningTasks(1)
        val componentInfo = runningTaskInfo.get(0).topActivity
        return componentInfo.className.toString().replace(myPackage+".views.","")
    }



}
