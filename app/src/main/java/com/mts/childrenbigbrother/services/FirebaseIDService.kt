package com.mts.childrenbigbrother.services

import com.google.firebase.iid.FirebaseInstanceIdService
import android.content.ContentValues.TAG
import com.google.firebase.iid.FirebaseInstanceId
import org.jetbrains.anko.defaultSharedPreferences


/**
 * Created by sakis on 6/24/2018.
 */
class FirebaseIDService : FirebaseInstanceIdService() {

    companion object {
        val BDK_REFRESHED_TOKEN = "boolean_refresh_token"
    }

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token

        val defaults = defaultSharedPreferences.edit().putBoolean(BDK_REFRESHED_TOKEN, true).commit()
    }
}