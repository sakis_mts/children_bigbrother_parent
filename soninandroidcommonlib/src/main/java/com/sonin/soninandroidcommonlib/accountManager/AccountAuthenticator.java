package com.sonin.soninandroidcommonlib.accountManager;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.sonin.android_common.networking.SessionManager;
import com.sonin.soninandroidcommonlib.R;
import com.sonin.soninandroidcommonlib.models.AccessToken;
import com.sonin.soninandroidcommonlib.networking.OAuthService;
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator;
import com.sonin.soninandroidcommonlib.utils.TangerineLog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.accounts.AccountManager.KEY_BOOLEAN_RESULT;

public class AccountAuthenticator extends AbstractAccountAuthenticator {

    private final Context           context;
    private AccountManager          accountManager;
    private SharedPreferences       sharedPreferences;
    private Class                   loginActivity;
    private String                  strEndPointUrl;

    public AccountAuthenticator(Context context , Class loginActivity, String strEndPointUrl) {
        super(context);

        this.loginActivity          = loginActivity;
        this.context                = context;
        this.accountManager         = AccountManager.get(context);
        sharedPreferences           = PreferenceManager.getDefaultSharedPreferences(context);
        this.strEndPointUrl         = strEndPointUrl;

    }

    /**
     *
     * @param response
     * @param accountType
     * @param authTokenType
     * @param requiredFeatures
     * @param options
     * @return
     * @throws NetworkErrorException
     */
    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {

        final Intent intent = new Intent(context, loginActivity);
        intent.putExtra(AccountGeneral.Companion.getARG_ACCOUNT_TYPE(),      accountType);
        intent.putExtra(AccountGeneral.Companion.getARG_AUTH_TYPE(),                       authTokenType);
        intent.putExtra(AccountGeneral.Companion.getARG_IS_ADDING_NEW_ACCOUNT(),       true);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,  response);

        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {

        // If the caller requested an authToken type we don't support, then
        // return an error
        if (!authTokenType.equals(AccountGeneral.Companion.getAUTHTOKEN_TYPE_READ_ONLY()) && !authTokenType.equals(AccountGeneral.Companion.getAUTHTOKEN_TYPE_FULL_ACCESS())) {

            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");

            return result;
        }

        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        final AccountManager am = AccountManager.get(context);

        String authToken = am.peekAuthToken(account, authTokenType);

        // Another try to authenticate the user
        if (TextUtils.isEmpty(authToken)) {
            final String password = am.getPassword(account);
            if (password != null) {
                try {
                    AccessToken accessToken = requestNewAccessToken(account);

                    if (accessToken!=null){

                        authToken = accessToken.getAccess_token();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        // If we get an authToken - we return it
        if (!TextUtils.isEmpty(authToken)) {

            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);

            return result;
        }

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our AuthenticatorActivity.
        final Intent intent = new Intent(context, loginActivity);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        intent.putExtra(AccountGeneral.Companion.getARG_ACCOUNT_TYPE(), account.type);
        intent.putExtra(AccountGeneral.Companion.getARG_AUTH_TYPE(), authTokenType);
        intent.putExtra(AccountGeneral.Companion.getARG_ACCOUNT_NAME(), account.name);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }


    @Override
    public String getAuthTokenLabel(String authTokenType) {

        if (AccountGeneral.Companion.getAUTHTOKEN_TYPE_FULL_ACCESS().equals(authTokenType)){

            return AccountGeneral.Companion.getAUTHTOKEN_TYPE_FULL_ACCESS_LABEL();

        }else if (AccountGeneral.Companion.getAUTHTOKEN_TYPE_READ_ONLY().equals(authTokenType)){

            return AccountGeneral.Companion.getAUTHTOKEN_TYPE_READ_ONLY_LABEL();

        }else{

            return authTokenType + " (Label)";

        }


    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {

        final Bundle result = new Bundle();
        result.putBoolean(KEY_BOOLEAN_RESULT, false);

        return result;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        return null;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    private AccessToken requestNewAccessToken(final Account account){

        final AccessToken[] accessToken = new AccessToken[1];
        OAuthService dsSmithEndpointApi =  RetrofitServiceGenerator.Companion.getServiceWithoutUsingAuthorization(OAuthService.class,strEndPointUrl, 10);

        String strRefreshToken      = accountManager.getPassword(account);
        String strCliendId          = context.getString(R.string.oauth_client_id);
        String strClient_secret     = context.getString(R.string.oauth_client_secret);

        Call<AccessToken> call  = dsSmithEndpointApi.refreshAccessToken(RetrofitServiceGenerator.Companion.getStrGrandTypeRefreshToken(),strRefreshToken,strCliendId,strClient_secret);
        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken>call, Response<AccessToken> response) {

                TangerineLog.Companion.log(new Gson().toJson(response));

                if (response.code()==200 ){

                    accountManager.setAuthToken(account,AccountGeneral.Companion.getAUTHTOKEN_TYPE_FULL_ACCESS(),response.body().getAccess_token());

                    TangerineLog.Companion.log("The authToken has been refreshed: "+ response.body().getAccess_token());
                    accessToken[0] = response.body();

                    //store the new access token
                    String strAuthToken = accessToken[0].getToken_type() +" "+ accessToken[0].getAccess_token();
                    accessToken[0].setAccess_token(strAuthToken);

                    SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

                    Gson gson   = new Gson();
                    String json = gson.toJson(accessToken[0]);
                    prefsEditor.putString(SessionManager.ACCESS_TOKEN_KEY, json);
                    prefsEditor.commit();

                }else{

                    TangerineLog.Companion.log("The authToken couldn't refreshed");

                }

            }

            @Override
            public void onFailure(Call<AccessToken>call, Throwable t) {
                // Log error here since request failed
                TangerineLog.Companion.log(t.toString(), "Retrofit Response -> Failure");
            }
        });

       return accessToken[0];
    }
}
