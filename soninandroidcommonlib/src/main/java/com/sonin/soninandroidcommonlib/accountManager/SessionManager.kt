package com.sonin.android_common.networking

import android.accounts.AccountManager
import android.content.Context
import android.preference.PreferenceManager
import android.app.Activity
import android.content.SharedPreferences
import android.content.Intent
import com.sonin.soninandroidcommonlib.accountManager.AccountGeneral
import com.sonin.soninandroidcommonlib.models.AccessToken
import com.sonin.soninandroidcommonlib.utils.AndroidUtils

public class SessionManager {

    var loggedInClass       :Class<out Activity>?   = null
    var loggedOutClass      :Class<out Activity>?   = null
    var sharedPrefs         :SharedPreferences?     = null
    private var strAlias    : String?               = null

    companion object {

        public val USERNAME             = "strUsername"
        public val LOGGEDIN_KEY         = "blnLoggedIn"
        const  val ACCESS_TOKEN_KEY     = "strAccessToken"
        const  val ACCESS_TOKEN_TYPE    = "strAccessTokenType"
        const  val AUTH_TOKEN_TYPE      = "strAuthorizationAccessToken"
        public val REFRESH_TOKEN_KEY    = "strRefreshToken"
        public val EXPIRY_DATE_KEY       = "dtExpiry"

        lateinit var instance : SessionManager

        fun init(context: Context, loggedInClass: Class<out Activity>, loggedOutClass: Class<out Activity>, strAlias: String?) {

            instance = SessionManager()
            instance.loggedInClass      = loggedInClass
            instance.loggedOutClass     = loggedOutClass
            instance.sharedPrefs        = PreferenceManager.getDefaultSharedPreferences(context)
            instance.strAlias           = strAlias

        }

    }

    fun getLoggedInUsername(): String {

        return sharedPrefs?.getString(USERNAME,"") ?: ""

    }

    fun setLoggedInUsername(strUsername: String) {

        sharedPrefs?.let {

            val prefEditor = it.edit()
            prefEditor.putString(USERNAME, strUsername)
            prefEditor.commit()
        }

    }

    fun login(context: Context, blnNavigateToHome:Boolean, blnStayLoggedIn: Boolean) {

        sharedPrefs?.let {

            val prefEditor = it.edit()
            prefEditor.putBoolean(LOGGEDIN_KEY, true)
            prefEditor.commit()
        }


        if (blnStayLoggedIn && loggedInClass!=null && loggedOutClass!=null && strAlias!= null){

            AndroidUtils.setActivityEnabled(context, loggedInClass, true)
            AndroidUtils.setActivityAliasEnabled(context, strAlias, false)

        }


        val applicationContext = context.applicationContext
        val intent = Intent(applicationContext, loggedInClass)


        if (blnNavigateToHome){

            context.startActivity(intent)

        }

    }

    fun login(context: Context, strAccessToken: String, strRefreshToken: String, dtExpiry: String, oath_type: String, blnNavigateToHome:Boolean, blnStayLoggedIn: Boolean) {

        sharedPrefs?.let {

            val prefEditor = it.edit()
            prefEditor.putBoolean(LOGGEDIN_KEY, true)
            prefEditor.putString(ACCESS_TOKEN_KEY, strAccessToken)
            prefEditor.putString(ACCESS_TOKEN_TYPE, oath_type)
            prefEditor.putString(AUTH_TOKEN_TYPE, oath_type+" "+strAccessToken )
            prefEditor.putString(REFRESH_TOKEN_KEY, strRefreshToken)
            prefEditor.putString(EXPIRY_DATE_KEY, dtExpiry)
            prefEditor.commit()

        }

        if (blnStayLoggedIn && loggedInClass!=null && loggedOutClass!=null && strAlias!= null){

            AndroidUtils.setActivityEnabled(context, loggedInClass, true)
            AndroidUtils.setActivityAliasEnabled(context, strAlias, false)
        }

        val applicationContext   = context.applicationContext
        val intent               = Intent(applicationContext, loggedInClass)

        if (blnNavigateToHome){

            context.startActivity(intent)

        }
    }

    fun setAccount( context: Context, objAccessToken: AccessToken, strAccountType: String, username: String, blnNavigateToHome: Boolean, blnStayLoggedIn: Boolean){

        val account = AccountGeneral.isAccountExists(AccountManager.get(context), username, strAccountType)

        if (account == null){

            AccountGeneral.createAccount(context, AccountManager.get(context),username, objAccessToken,strAccountType, blnNavigateToHome,blnStayLoggedIn)

            val sessionManager  = SessionManager.instance
            val intExpires      = objAccessToken.expires_in

            sessionManager.setLoggedInUsername(username)
            sessionManager.login(context, objAccessToken.access_token!!, objAccessToken.refresh_token!!, Integer.toString(intExpires!!), objAccessToken.token_type!!, blnNavigateToHome,blnStayLoggedIn)

        }else{

            AccountGeneral.updateAccessToken(AccountManager.get(context),objAccessToken, username,strAccountType)

            val sessionManager  = SessionManager.instance
            val intExpires      = objAccessToken.expires_in

            sessionManager.setLoggedInUsername(username)
            sessionManager.login(context, objAccessToken.access_token!!, objAccessToken.refresh_token!!, Integer.toString(intExpires!!), objAccessToken.token_type!!, blnNavigateToHome,blnStayLoggedIn)

        }
    }

    fun logout(context: Context ){
        sharedPrefs?.let {

            val prefEditor = it.edit()
            prefEditor.clear()
            prefEditor.commit()

        }


        if (loggedInClass!=null && loggedOutClass!=null && strAlias!= null){

            AndroidUtils.setActivityEnabled(context, loggedInClass, false)
            AndroidUtils.setActivityAliasEnabled(context, strAlias, true)
        }

        val applicationContext   = context.applicationContext
        val intent               = Intent(applicationContext, loggedOutClass)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
        (context as Activity).finish()
    }

    fun getAccessToken(): String                    = sharedPrefs?.getString(ACCESS_TOKEN_KEY, "") ?: ""
    fun getAccessTokenType(): String                = sharedPrefs?.getString(ACCESS_TOKEN_TYPE, "") ?: ""
    fun getAuthorizationAccessTokenType(): String   = sharedPrefs?.getString(AUTH_TOKEN_TYPE, "") ?: ""
    fun isLoggedIn(): Boolean                       = sharedPrefs?.getBoolean(LOGGEDIN_KEY, false) ?: false

}