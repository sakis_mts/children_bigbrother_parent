package com.sonin.soninandroidcommonlib.validation;

public class PasswordPolicy extends Policy
{
	
	private PasswordStrength passwordStrength;

	public enum PasswordStrength {
		WEAK, MEDIUM, STRONG, VERY_STRONG
	}

    private PasswordPolicy()
    {

    }


	public PasswordPolicy(PasswordStrength passwordStrength) {

		this.blnValidateEmpty = true;
		this.blnValidateMin = false;
		this.blnValidateMax = false;
		this.passwordStrength = passwordStrength;

	}

	@Override
	public boolean validatePolicy(Field field) {

		if(super.validatePolicy(field)==false){
			return false;
		}

		PasswordStrength calculatedStrength = ValidationUtils.calculateStrength(field.getString());
		if(calculatedStrength.ordinal() < passwordStrength.ordinal())
		{
			field.setStrFailure("Password Strength is too weak");
			return false;
		}

		return true;
	}


}
