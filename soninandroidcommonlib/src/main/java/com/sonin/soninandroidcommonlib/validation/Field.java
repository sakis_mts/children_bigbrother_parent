package com.sonin.soninandroidcommonlib.validation;

import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.sonin.soninandroidcommonlib.customViews.PhoneView;

public class Field {
	
	public enum FieldType { NUMBER, TEXT, PHONE, EMAIL, PASSWORD};

    private View 		view;
	private FieldType   fieldType;

	private Policy 		fieldPolicy;
	private String      strFailure;
	private String      requestKey;
	
	
	private Field(FieldBuilder builder)
    {
       	view   			= builder.view;
        fieldType 		= builder.fieldType;

        fieldPolicy 	= builder.fieldPolicy;
        strFailure  	= builder.strFailure;
        requestKey     	= builder.requestKey;
    }
	
	public boolean validateField()
	{
		
		String strValidate = getString();
		
		switch(fieldType){
			case NUMBER:
				
				if(ValidationUtils.validateNumeric(strValidate) == false)
				{
					return false;
				}
				
				break;
			case PHONE:
				
				break;
			case EMAIL:
				
				if(ValidationUtils.validateEmail(strValidate) == false)
				{
                    this.strFailure = "Invalid Email";
					return false;
				}
				
				break;
		
		}
		
		if(fieldPolicy.validatePolicy(this)==false){
			return false;
		}
		
		
		
		return true;
	}


	public String getString()
	{
		String string = "";

		if(view instanceof TextView)
		{
			string = ((TextView) view).getText().toString();
		}else if(view instanceof Spinner)
		{
			string = ((Spinner) view).getSelectedItem().toString();
		}else if (view instanceof PhoneView){

			if (((PhoneView)view).validatePhoneNumber()!=null){

				string = ((PhoneView)view).validatePhoneNumber();
			}

		}

		return string;
	}


	public void setPolicy(Policy policy){
		this.fieldPolicy = policy;
	}

	public View getView(){return this.view;}

	public String getStrFailure() {
		return strFailure;
	}

	public void setStrFailure(String strFailure) {
		this.strFailure = strFailure;
	}

	public String getRequestKey() {
		return requestKey;
	}

	public void setRequestKey(String requestKey) {
		this.requestKey = requestKey;
	}
	



    public static class FieldBuilder
    {
        private View		view;
        private FieldType   fieldType;

        private Policy fieldPolicy 		= new DefaultNotEmptyPolicy();
        private String      strFailure  = "Invalid Field";
        private String      requestKey  = null;

        public FieldBuilder(View view, FieldType type)
        {
            this.view   	= view;
            this.fieldType 	= type;
        }

        public FieldBuilder policy(Policy policy)
        {
            this.fieldPolicy = policy;
            return this;
        }

        public FieldBuilder failureMessage(String message)
        {
            this.strFailure = message;
            return this;
        }


        public FieldBuilder requestKey(String requestKey)
        {
            this.requestKey = requestKey;
            return this;
        }

        public Field createField()
        {
            return new Field(this);
        }
    }
}
