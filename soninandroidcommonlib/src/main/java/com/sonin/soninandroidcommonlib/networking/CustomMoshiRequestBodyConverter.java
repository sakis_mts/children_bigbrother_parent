package com.sonin.soninandroidcommonlib.networking;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonWriter;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Converter;
import com.squareup.moshi.JsonWriter;

final class CustomMoshiRequestBodyConverter<T> implements Converter<T, RequestBody> {
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");

    private final JsonAdapter<T> adapter;
    private final boolean serializeNulls;

    CustomMoshiRequestBodyConverter(JsonAdapter<T> adapter,  boolean serializeNulls) {
        this.adapter = adapter;
        this.serializeNulls = serializeNulls;
    }

    @Override public RequestBody convert(T value) throws IOException {
        Buffer buffer = new Buffer();
        JsonWriter writer = JsonWriter.of(buffer);
//        adapter.toJson(writer, value);
        writer.setSerializeNulls(serializeNulls);
        adapter.toJson(writer, value);
        return RequestBody.create(MEDIA_TYPE, buffer.readByteString());
    }
}
