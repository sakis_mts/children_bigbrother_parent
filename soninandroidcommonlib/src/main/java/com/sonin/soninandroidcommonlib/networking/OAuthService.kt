package com.sonin.soninandroidcommonlib.networking


import com.sonin.soninandroidcommonlib.models.AccessToken
import retrofit2.http.Field
import retrofit2.http.POST
import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded

interface OAuthService {

    // Auth
    @FormUrlEncoded
    @POST("/api/login")
    fun getAccessTokennObservable(
            @Field("grant_type") grant_type: String,
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String): Observable<AccessToken>


    // Auth
    @FormUrlEncoded
    @POST("/oauth/token")
    fun getAccessToken(
            @Field("grant_type") grant_type: String,
            @Field("username") email: String,
            @Field("password") password: String,
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String): Observable<AccessToken>

    @FormUrlEncoded
    @POST("/oauth/token")
    fun refreshAccessToken(
            @Field("grant_type") grantType: String,
            @Field("refresh_token") refreshToken: String,
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String): Call<AccessToken>

    @FormUrlEncoded
    @POST("/oauth/token")
    fun refreshAccessTokenObservable(
            @Field("grant_type") grantType: String,
            @Field("refresh_token") refreshToken: String,
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String): Observable<AccessToken>

    @POST("/password/email")
    fun changePassword(@Body jsonUsername: RequestBody): Call<ResponseBody>

    @POST("/password/email")
    fun changePasswordObservable(@Body jsonUsername: RequestBody): Observable<ResponseBody>
}