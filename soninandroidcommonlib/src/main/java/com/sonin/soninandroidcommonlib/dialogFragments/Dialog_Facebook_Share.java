package com.sonin.soninandroidcommonlib.dialogFragments;



import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sonin.soninandroidcommonlib.R;
import com.sonin.soninandroidcommonlib.utils.FilesystemUtils;


public class Dialog_Facebook_Share extends DialogFragment implements
		OnClickListener, FilesystemUtils.OnBitmapLoadedListener {

	private ImageView ivImage;
	private TextView  tvText;
	private TextView  tvSuccess;
	private TextView  tvFailure;
	
	
	private Uri bitmapURI;
	private String main;
	private String success;
	private String failure;
	
	public  static String  BUNDLE_KEY_BITMAP   = "bundle_key_image";
	public  static String  BUNDLE_KEY_MAIN     = "bundle_key_main";
	public  static String  BUNDLE_KEY_SUCCESS  = "bundle_key_success";
	public  static String  BUNDLE_KEY_FAILURE  = "bundle_key_failure";

	public interface GenericDialogListener {
		void onDialogSuccess();
		void onDialogFailure();
	}
	
public static Dialog_Facebook_Share newInstance(GenericDialogListener listener, String text, String success, String failure) {
		
		Bundle args = new Bundle();

		args.putString(BUNDLE_KEY_MAIN, text);
		args.putString(BUNDLE_KEY_SUCCESS, success);
		args.putString(BUNDLE_KEY_FAILURE, failure);
		
		Dialog_Facebook_Share f = new Dialog_Facebook_Share();
		f.setArguments(args);
		if(listener instanceof Fragment){
			f.setTargetFragment((Fragment) listener, /* requestCode */1234);
		}
		
		return f;
	}

	public static Dialog_Facebook_Share newInstanceWithPhoto(GenericDialogListener listener, String bitmapUri, String text, String success, String failure) {
		
		Bundle args = new Bundle();
		args.putString(BUNDLE_KEY_BITMAP , bitmapUri);
		args.putString(BUNDLE_KEY_MAIN,    text);
		args.putString(BUNDLE_KEY_SUCCESS, success);
		args.putString(BUNDLE_KEY_FAILURE, failure);
		
		Dialog_Facebook_Share f = new Dialog_Facebook_Share();
		f.setArguments(args);
		if(listener instanceof Fragment){
			f.setTargetFragment((Fragment) listener, /* requestCode */1234);
		}
		
		return f;
	}

	public Dialog_Facebook_Share() {
		// Empty constructor required for DialogFragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.dialog_fragment_facebook, container);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		initVars(view);
		setData();
		return view;
	}

	

	private void initVars(View view) {
		
		tvText     = (TextView)  view.findViewById(R.id.tvText);
		ivImage    = (ImageView) view.findViewById(R.id.ivSharePhoto);
		tvSuccess  = (TextView)  view.findViewById(R.id.tvDialogSuccess);
		tvFailure  = (TextView)  view.findViewById(R.id.tvDialogFailure);
		
		tvSuccess.setOnClickListener(this);
		tvFailure.setOnClickListener(this);

		Bundle args = getArguments();
		
		
		main    = args.getString(BUNDLE_KEY_MAIN);
		success = args.getString(BUNDLE_KEY_SUCCESS);
		failure = args.getString(BUNDLE_KEY_FAILURE);
		
		if(args.containsKey(BUNDLE_KEY_BITMAP)){
			ivImage.setVisibility(View.VISIBLE);
			bitmapURI  = Uri.parse(args.getString(BUNDLE_KEY_BITMAP));
			FilesystemUtils.getBitmapFromFilesystem(getActivity(), bitmapURI, this);
		}

		
	}
	

	private void setData(){
		
		tvText.setText(main);
		tvSuccess.setText(success);
		tvFailure.setText(failure);

	}
	

	@Override
	public void onClick(View v) {
		v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		int id = v.getId();
		if (id == R.id.tvDialogSuccess) {
			if(getTargetFragment()!=null){
				Fragment fragment = (Fragment) getTargetFragment();
				((GenericDialogListener) fragment).onDialogSuccess();
			}else{
				FragmentActivity activity = getActivity();
				((GenericDialogListener) activity).onDialogSuccess();
			}
			this.dismiss();
		} else if (id == R.id.tvDialogFailure) {
			if(getTargetFragment()!=null){
				Fragment fragment = (Fragment) getTargetFragment();
				((GenericDialogListener) fragment).onDialogFailure();
			}else{
				FragmentActivity activity = getActivity();
				((GenericDialogListener) activity).onDialogFailure();
			}
			this.dismiss();
		}

	}

	@SuppressLint("NewApi")
	@Override
	public void onBitmapLoaded(Bitmap bitmap) {
		
		if(bitmap!=null){
			
			double width  = bitmap.getWidth();
			double height = bitmap.getHeight();
			double aspectRatio = height/width;
			
			double newHeight       = aspectRatio * ivImage.getLayoutParams().width;
			int   newHeightRounded = (int) Math.round(newHeight);
			
			ivImage.setLayoutParams(new RelativeLayout.LayoutParams(ivImage.getLayoutParams().width, newHeightRounded));
			
			if (Build.VERSION.SDK_INT >= 16) {
				
				ivImage.setBackground(new BitmapDrawable(getResources(), bitmap));
			}else{
				ivImage.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
			}
			
		
		}
		
		
		
		
	}


	

}
