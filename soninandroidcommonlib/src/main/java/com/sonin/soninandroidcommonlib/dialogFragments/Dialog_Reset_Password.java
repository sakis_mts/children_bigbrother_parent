package com.sonin.soninandroidcommonlib.dialogFragments;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.sonin.soninandroidcommonlib.R;
import com.sonin.soninandroidcommonlib.validation.DefaultNotEmptyPolicy;
import com.sonin.soninandroidcommonlib.validation.Field;
import com.sonin.soninandroidcommonlib.validation.Form;

public class Dialog_Reset_Password extends DialogFragment implements
		OnClickListener {

	private TextView tvTitle;
	private EditText etEmail;
	private TextView tvSuccess;
	private TextView tvFailure;
	
	private ProgressDialog pgD;

	private Form form;
	
	private Activity mActivity;

	public interface GenericDialogListener {
		void onDialogSuccess();
		void onDialogFailure();
	}

	public static Dialog_Reset_Password newInstance() {
		
	
		
		Dialog_Reset_Password f = new Dialog_Reset_Password();
		
		return f;
	}

	public Dialog_Reset_Password() {
		// Empty constructor required for DialogFragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.dialog_fragment_reset, container);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		initVars(view);
		return view;
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

	

	private void initVars(View view) {
		
		tvTitle    = (TextView) view.findViewById(R.id.tvDialogTitle);
		etEmail    = (EditText) view.findViewById(R.id.etResetEmail);
		tvSuccess  = (TextView) view.findViewById(R.id.tvDialogSuccess);
		tvFailure  = (TextView) view.findViewById(R.id.tvDialogFailure);

		

		
		form = new Form();
		form.setCustomFailureAnimation(AnimationUtils.loadAnimation(mActivity,R.anim.scale_pop_animation));
		form.addField(new Field.FieldBuilder(etEmail, Field.FieldType.EMAIL).policy(new DefaultNotEmptyPolicy()).createField());
		


		tvSuccess.setOnClickListener(this);
		tvFailure.setOnClickListener(this);
		
		
		
	}
	

	

	@Override
	public void onClick(View v) {
		v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		int id = v.getId();
		if (id == R.id.tvDialogSuccess) {
			if(form.validateForm()){
				
				
				resetPassword();
			}
			this.dismiss();
		} else if (id == R.id.tvDialogFailure) {
			this.dismiss();
		}

	}

	private void resetPassword() {
		

		HashMap<String, String> postParams = new HashMap<String, String>();
		postParams.put("strAction", "forgotPassword");
		postParams.put("strEmail", etEmail.getText().toString());

		

	}
	
	


	

}
